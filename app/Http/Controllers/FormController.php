<?php

namespace App\Http\Controllers;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use SoapClient;
use Validator;
use ReCaptcha\ReCaptcha;
use ReCaptcha\RequestMethod\SocketPost ;
use ReCaptcha\RequestMethod\CurlPost ;
use Mail;

class FormController extends Controller
{


    public function send(Request $request)
    {

       
       $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);

      
        $name = $request->name; 
        $email = $request->email; 
        $mobile = $request->mobile; 
        $service = $request->service; 
        $message1 = $request->message1; 

        $remoteip = $request->ip();
        $secretKey = env('NOCAPTCHA_SECRET');
        $recaptchaCode = $request['g-recaptcha-response'];
        
        $recaptcha = new ReCaptcha($secretKey);
        $resp = $recaptcha->verify($recaptchaCode,new CurlPost());
    	if(!$resp->isSuccess())
    	{
    	   return Redirect::back()->with('msg', 'Select Captcha');
    	}

        
        $options = array(
                'location' => 'http://hem.njcrm.in/soap.php',
                'uri' => 'http://hem.njcrm.in',
                'trace' => 1
                );

        $user_auth = array(
                'user_name' => 'Backoffice.sales',
                'password' => md5('Heminfo@315'),
                'version' => '.01'
                );

                $client = new SoapClient(NULL, $options);
                $response = $client->login($user_auth,'Heminfo@315');
                $session_id = $response->id;
                $user_id = $client->get_user_id($session_id);
                $response = $client->set_entry($session_id,'Leads', array(
                        array("name" => 'last_name',"value" => $name),
                        array("name" => 'email1',"value" => $email),
                        array("name" => 'phone_mobile',"value" => $mobile),
                        array("name" => 'description',"value" => $message1),
                        array("name" => 'service_c',"value" => $service),
                        array("name" => 'lead_source',"value" => 'Web Site'),
                        array("name" => 'team_id',"value" => '1'),
                        array("name" => 'team_set_id',"value" => '1'),
                        array("name" => 'assigned_user_id',"value" => $user_id)
                       ));

                return $response;


               // return redirect('thank-you.html') ;

                   
            }


        }
