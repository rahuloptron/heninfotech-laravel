<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index', ['title' => 'Hem Infotech IT Solution Service Provider']);
});

Route::get('/index.html', function () {
    return view('index', ['title' => 'Hem Infotech IT Solution Service Provider']);
});

Route::get('/thank-you.html', function () {
    return view('thank-you', ['title' => 'Thank You']);
});

Route::get('/products/index.html', function () {
    return view('products/index', ['title' => 'Products of HemInfotech']);
});

Route::get('/services/index.html', function () {
    return view('services/index', ['title' => 'Services of HemInfotech']);
});

Route::get('/solutions/index.html', function () {
    return view('solutions/index', ['title' => 'Solutions of HemInfotech']);
});

Route::get('/about.html', function () {
    return view('about', ['title' => 'About of HemInfotech']);
});

Route::post('/send', 'FormController@send');

Route::get('/contact.html', function () {
    return view('contact', ['title' => 'Contact of HemInfotech']);
});

Route::get('/services/amc-services.html', function () {
    return view('services/amc-services', ['title' => 'Amc Services in Gujrat']);
});

Route::get('/services/computer-repair-services.html', function () {
    return view('services/computer-repair-services', ['title' => 'Computer Repair Services in Gujrat']);
});

Route::get('/solutions/antivirus-dealer.html', function () {
    return view('solutions/antivirus-dealer', ['title' => 'Antivirus Dealer in Gujrat']);
});

Route::get('/products/antivirus-dealer.html', function () {
    return view('products/antivirus-dealer', ['title' => 'Antivirus Dealer in Gujrat']);
});


Route::get('/services/data-recovery.html', function () {
    return view('services/data-recovery', ['title' => 'Data Recovery Serives in Gujrat']);
});

Route::get('/solutions/databackup.html', function () {
    return view('solutions/databackup', ['title' => 'Best Data Backup Solutions']);
});

Route::get('/products/firewall.html', function () {
    return view('products/firewall', ['title' => 'Firewall Security Services']);
});

Route::get('/solutions/networking.html', function () {
    return view('solutions/networking', ['title' => 'Computer Network Service Providers']);
});

Route::get('/services/resident-engineer.html', function () {
    return view('services/resident-engineer', ['title' => 'Computer Network Service Providers']);
});

Route::get('/products/nas.html', function () {
    return view('products/nas', ['title' => 'Computer Network Service Providers']);
});

Route::get('/products/servers-and-workstations.html', function () {
    return view('products/servers-and-workstations', ['title' => 'Servers and Workstations Dealer']);
});

Route::get('/products/desktop.html', function () {
    return view('products/desktop', ['title' => 'Desktop Dealer']);
});

Route::get('/products/cctv-on-rent.html', function () {
    return view('products/cctv-on-rent', ['title' => 'CCTV On Rent']);
});

Route::get('/solutions/thin-client.html', function () {
    return view('solutions/thin-client', ['title' => 'Thin Client']);
});

Route::get('/privacy-policy.html', function () {
    return view('privacy-policy', ['title' => 'Hem Infotech | Privacy Policy']);
});

Route::get('/terms-and-conditions.html', function () {
    return view('terms-and-conditions', ['title' => 'Hem Infotech | Terms and Conditions']);
});

Route::get('/refund-cancellation-policy.html', function () {
    return view('refund-cancellation-policy', ['title' => 'Hem Infotech | Refund Cancellation Policy']);
});

Route::get('/clientele.html', function () {
    return view('clientele', ['title' => 'Clientele']);
});

Route::get('/careers.html', function () {
    return view('career', ['title' => 'Careers - HemInfotech']);
});

Route::get('/support-request.html', function () {
    return view('support-request', ['title' => 'Support Request']);
});


Route::get('/welcome', function () {
    return view('welcome');
});



//Auth::routes();


