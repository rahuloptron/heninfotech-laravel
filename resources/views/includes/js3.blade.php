<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../js/universal/jquery.js"></script>

<!-- animations -->
<script src="../js/animations/js/animations.min.js" type="text/javascript"></script>

<!-- mega menu -->
<script src="../js/mainmenu/bootstrap.min.js"></script> 
<script src="../js/mainmenu/customeUI.js"></script> 

<!-- MasterSlider -->
<script src="../js/masterslider/jquery.easing.min.js"></script>
<script src="../js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

    var slider = new MasterSlider();
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('bullets');

     slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:580,   // slider standard height
         space:0,
         speed:45,
         loop:true,
         preload:0,
         autoplay:true,
         view:"basic"
    });

})(jQuery);
</script>

<!-- scroll up -->
<script src="../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../js/mainmenu/modernizr.custom.75180.js"></script>

<!-- Accordion-->
<script type="text/javascript" src="../js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="../js/accordion/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../js/accordion/custom.js"></script>

<!-- owl carousel -->
<script src="../js/carouselowl/owl.carousel.js"></script>

<script type="text/javascript" src="../js/universal/custom.js"></script>

<!-- animate number -->
<script src="../js/aninum/jquery.animateNumber.min.js"></script>