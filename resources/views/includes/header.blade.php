
    <div id="topHeader">
    
    <div class="wrapper">
         
        <div class="top_nav">
        <div class="container">
            
            <div class="left">
            
                <ul class="topsocial">
                <li><a href="https://www.facebook.com/HemInfo/"><i class="fa fa-facebook"></i></a></li>
                
                <li><a href="https://in.linkedin.com/in/hem-infotech-a4353233"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>
            
            </div><!-- end left links -->
            
            <div class="right">
                
                Call Us: <strong>(+91) 966 254 7607</strong> Email: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>
          
        </div><!-- end right links -->
        
        </div>
        </div>
            
    </div>
    
    </div><!-- end top navigations -->
    
    
    <div id="trueHeader">
    
    <div class="wrapper">
    
     <div class="container">
    
        <!-- Logo -->
        <div class="logo"><a href="/index.html" id="logo"></a></div>
        
    <!-- Navigation Menu -->
    <nav class="menu_main">
        
    <div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1">
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
           <li><a href="/index.html">Home</a></li>
               
         <li><a href="/about.html">About</a></li>
        
        <li class="dropdown"><a href="services/index.html" data-toggle="dropdown" class="dropdown-toggle">Services</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="/services/amc-services.html">AMC Service</a></li>
                <li><a href="/services/data-recovery.html">Data Recovery</a></li>
                <li><a href="/services/computer-repair-services.html">Computer Repair Services</a></li>
                <li><a href="/services/resident-engineer.html">Resident Engineer</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="solutions/index.html" data-toggle="dropdown" class="dropdown-toggle">Solutions</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="/solutions/databackup.html">Data Backup and Storage</a></li>
                <li><a href="/solutions/antivirus-dealer.html">Antivirus And Firewall</a></li>
                <li><a href="/solutions/thin-client.html">Thin Client</a></li>
                <li><a href="/solutions/networking.html">Active Passive Networking</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="products/index.html" data-toggle="dropdown" class="dropdown-toggle">Products</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="/products/antivirus-dealer.html">Antivirus</a></li>
                <li><a href="/products/desktop.html">Desktops</a></li>
                <li><a href="/products/servers-and-workstations.html">Server And Workstations</a></li>
                <li><a href="/products/firewall.html">Firewall</a></li>
                <li><a href="/products/nas.html">NAS</a></li>
                <li><a href="/products/cctv-on-rent.html">CCTV on hire</a></li>
            </ul>
        </li>
        
        <li> <a href="/contact.html">Contact Us</a> </li>
        
        </ul>
        
      </div>
      </div>
     </div>
     
    </nav><!-- end Navigation Menu -->
        
    </div>
        
    </div>
    
    </div>
    
