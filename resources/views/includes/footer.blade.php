<div class="footer2">

<div class="footersecs">
<div class="container">
    
    <div class="one_fourth">
            <h3 class="white">About <strong>Us</strong></h3>
      <p>We work inexhaustibly to provide the perfect solutions by putting our efficiencies to meeting your business needs. Our aim is to offer quality through our well-thought solutions, competent technology, and loyal partnership. We want to create long-lasting associations and become the best choice for information technology partners of our clients.</p>
        
    </div><!-- end address -->
    
    <div class="one_fourth">
        
        <h3 class="white">Quick Links</h3>
        
        <ul class="uselinks">
        
        <li><a href="/index.html"><i class="fa fa-angle-right"></i> Home</a></li>
        <li><a href="/about.html"><i class="fa fa-angle-right"></i> About</a></li>
        <li><a href="/services/data-recovery.html"><i class="fa fa-angle-right"></i> Data Recovery</a></li>
        <li><a href="/services/amc-services.html"><i class="fa fa-angle-right"></i> AMC Service</a></li>
        <li><a href="/services/computer-repair-services.html"><i class="fa fa-angle-right"></i> Computer Repair</a></li>
        <li><a href="/products/firewall.html"><i class="fa fa-angle-right"></i> Firewall</a></li>
        <li><a href="/products/clientele.html"><i class="fa fa-angle-right"></i> Clients</a></li>
        <li><a href="/careers.html"><i class="fa fa-angle-right"></i> Careers</a></li>
        <li><a href="/support-request.html"><i class="fa fa-angle-right"></i> Support Request</a></li>
            
                
        </ul>
        
    </div><!-- end useful links -->
    
    <div class="one_fourth">
        
        <h3 class="white">Our Solutions</h3>
        
        <ul class="uselinks">
        
        <li><a href="/solutions/databackup.html"><i class="fa fa-angle-right"></i> Data Backup</a></li>
        <li><a href="/solutions/antivirus-dealer.html"><i class="fa fa-angle-right"></i> Antivirus</a></li>
        <li><a href="/solutions/thin-client.html"><i class="fa fa-angle-right"></i> Thin Client</a></li>
        <li><a href="/solutions/networking.html"><i class="fa fa-angle-right"></i> Networking</a></li>
         <li><a href="#"><i class="fa fa-angle-right"></i> Download Profile</a></li>
      
        </ul>
        
    </div><!-- end tweets -->
    
    <div class="one_fourth last">
        

          <h3 class="white">Address</h3>
        <ul class="address">
        
            
            <li><i class="fa fa-map-marker fa-lg"></i>315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, Ahmedabad, Gujarat 380006</li>
            <li><i class="fa fa-envelope"></i> sales@heminfotech.com </li>
            <li><i class="fa fa-phone-square"></i> +91 966 254 7607</li>
            <li><i class="fa fa-whatsapp" aria-hidden="true"></i> +91 937 416 4942</li>
            
            
        </ul>
        
    </div><!-- end flickr -->
    
    
</div>
</div><!-- end four sections -->

 </div>

 <div class="copyrights">
<div class="container">

    <div class="one_half">Copyright © 2018 Hem Infotech. All Rights Reserved</div>
    
    <div class="one_half last"><a href="/privacy-policy.html">Privacy Policy</a> | <a href="/terms-and-conditions.html"> Terms & Conditions</a> | <a href="/refund-cancellation-policy.html">Refund Policy</a>| <a href="/contact.html">Contact</a></div>
    
    
</div>
</div><!-- end copyrights -->