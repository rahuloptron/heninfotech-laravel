@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/Parallax1.jpg);">
<div class="container">

    <h1 class="white"><strong>About Us</strong></h1>
    
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <div class="two_third">

    <h3>Who We Are</h3>
        
    <p class="big_text3">We work inexhaustibly to provide the perfect solutions by putting our efficiencies to meeting your business needs. Our aim is to offer quality through our well-thought solutions, competent technology, and loyal partnership. We want to create long-lasting associations and become the best choice for information technology partners of our clients.</p>

    <div class="margin_top2"></div>

    <p class="big_text3">We work inexhaustibly to provide the perfect solutions by putting our efficiencies to meeting your business needs. Our aim is to offer quality through our well-thought solutions, competent technology, and loyal partnership. We want to create long-lasting associations and become the best choice for information technology partners of our clients.</p>

    </div>

    <div class="one_third last">

         <h3>What we do</h3>

       <ul class="list9">
           <li><i class="fa fa-long-arrow-right"></i> <a href="/services/amc-services.html">Amc and Repair Services</a></li>
           <li><i class="fa fa-long-arrow-right"></i> <a href="/solutions/networking.html">Networking Solutions</a></li>
           <li><i class="fa fa-long-arrow-right"></i>  <a href="/products/cctv-on-rent.html">Surveillance and Solutions</a></li>
           <li><i class="fa fa-long-arrow-right"></i> <a href="/solutions/antivirus-dealer.html">Antivirus and Firewall</a></li>
           <li><i class="fa fa-long-arrow-right"></i> <a href="/solutions/databackup.html">Data Backup and Storage</a></li>
           <li><i class="fa fa-long-arrow-right"></i> <a href="/solutions/thin-client.html">Thin Client</a></li>

       </ul>
    </div>
    
    
</div><!-- end section -->

<div class="clearfix"></div>

<div class="container tbp3">
<div class="container">
    
    <div class="title11">
        <h2>Key <strong>HighLights</strong>
        <span class="line4"></span></h2>
    </div>
    
    <br>
    
    
    
    <div class="one_half">
        
        <ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Established in 2002, Hem Infotech has computing and well organised IT infrastructure as the primary focus.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> We're proud of our technical team that has 20+ well experienced & certified individuals with the focus on finance, hospitality, education, government and more.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> We are proud OEM Service Partner and business associate for award-winning brands. HP, NComputing, Enjay, Qnap, Cyberoam, Quick Heal are some of them.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> Running a business in the era of technology without computers is a futile exercise. We play a crucial role in any business process as an IT Services company.</li>
                        
                    </ul>
                

    </div>

    <div class="one_half last">
        
        <ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> We are presently keeping up 20000+ hardware and over 1000 customers.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> We hold specialization in five services - AMC Services, Thin Clients, CRM, Public Wifi and IT Solution Integration. We are devoted to attaining lasting dependability.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> Hem Infotech is one of the rare companies in Gujarat that utilizes CRM based systematic support mechanism for best applicable assistance.</li>
                         <div class="margin_top2"></div><div class="clearfix"></div>
                        <li><i class="fa fa-long-arrow-right"></i> We are a significant part of our client’s IT infrastructure because we also offer the functionality of auditing it. We maintain regular and consistent communication with them.</li>
                      
                    </ul>
                
        
    </div>

</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec17">
<div class="container">

     <div class="title8">
    
        <h2><span class="line"></span><span class="text">Core <strong>Team</strong><span></span></span></h2>
    
    </div><!-- end section title heading -->

      <br>
    
    <div class="one_half">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>PURAV SHAH</strong></h5>
                <em>CEO</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.software like versions.software like versions.</p>
                
                
            </div>
            
            <img src="images/attorney-1.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>BRIJESH PAREKH</strong></h5>
                <em>SENIOR TECHNICAL HEAD</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.software like versions.software like versions.</p>
              
            </div>
            
            <img src="images/attorney-2.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    
    
    </div><!-- end section -->
    

</div>
</div>


<div class="clearfix"></div>

<div class="feature_sec13">
<div class="container">
    
    <div class="one_half">
    
        <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Eagle Corporation</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Nigam Amin, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- R. R. Kapadia</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- J.M.Chauhan, The Bar Council of Gujarat</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Confedration of Indian Industry (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- C.P. Mathur, Employees Provident Fund Organisation</strong></div>
                   
            </div><!-- end section -->
           
        </div>
        
    </div><!-- end all section -->
    
    
     <div class="one_half last">
    
        <h3><strong>Industry Verticals</strong></h3>
        
                    <ul class="list9">
                    <li><i class="fa fa-long-arrow-right"></i> School and Colleges</li>
                    <li><i class="fa fa-long-arrow-right"></i> Government</li>
                    <li><i class="fa fa-long-arrow-right"></i> Real Estate</li>
                    <li><i class="fa fa-long-arrow-right"></i> Hospitality</li>
                    <li><i class="fa fa-long-arrow-right"></i> Manufacturing</li>
                    <li><i class="fa fa-long-arrow-right"></i> Architects and Graphics</li>
                       
                    </ul>

    </div><!-- end all section -->
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>

<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>

<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop
