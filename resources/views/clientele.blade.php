@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/Parallax1.jpg);">
<div class="container">

    <h1 class="white"><strong>Clientele</strong></h1>
    
</div>
</div>


<div class="content_fullwidth">
<div class="container">
	
<div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/1.png" alt="">
            </div>
            
           
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/2.jpg" alt="">
            </div>
            
           
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/3.png" alt="">
            </div>
            
           
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/4.png" alt="">
            </div>
            
         
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/5.png" alt="">
            </div>
         
        </div>
    
    </div><!-- end section -->

    
    <div class="clearfix margin_top3"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/setu.jpg" alt="">
            </div>
          
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/7.png" alt="">
            </div>
          
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/8.png" alt="">
            </div>
         
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/9.jpg" alt="">
            </div>
            
          
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gayatri-micron.jpg" alt="">
            </div>
          
            
        </div>
    
    </div><!-- end section -->

    
        <div class="clearfix margin_top3"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/11.jpg" alt="">
            </div>
          
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/12.png" alt="">
            </div>
         
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/13.png" alt="">
            </div>
            
            
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/14.jpg" alt="">
            </div>
            
        
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gsec.jpg" alt="">
            </div>
          
            
        </div>
    
    </div><!-- end section -->

    
   
</div>
</div>

@stop