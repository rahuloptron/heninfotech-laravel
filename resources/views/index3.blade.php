<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Hem Infotech IT Solution Service Provider</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

    <!-- animations -->
    <link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
  
    
    <!-- mega menu -->
    <link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- MasterSlider -->
	<link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="js/masterslider/skins/default/style.css" />
    
    <!-- owl carousel -->
    <link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">
    
    <!-- forms -->
    <link rel="stylesheet" href="js/form/sky-forms.css" type="text/css" media="all">
    
    <!-- tabs -->
    <link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs2.css">

    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
    
</head>

<body>

<div class="wrapper_boxed">

<div class="site_wrapper">

<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_nav">
        <div class="container">
        	
            <div class="left">
            
            	<ul class="topsocial">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            
            </div><!-- end left links -->
            
            <div class="right">
                
                Call Us: <strong>+91 966 254 7607</strong>       Email: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>
          
        </div><!-- end right links -->
        
        </div>
        </div>
            
 	</div>
    
	</div><!-- end top navigations -->
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
	<nav class="menu_main">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
         <li class="dropdown"><a href="index.html" data-toggle="dropdown" class="dropdown-toggle">home</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="index2.html">home2</a></li>
                <li><a href="index3.html">home3</a></li>
              
            </ul>
        </li>
               
         <li><a href="about.html">About</a></li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Products</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="attorneys.html">Antivirus</a></li>
                <li><a href="attorneys-2.html">Desktops</a></li>
                <li><a href="attorneys-3.html">Server And Workstations</a></li>
                <li><a href="attorneys-4.html">Firewall</a></li>
                <li><a href="attorneys-fullbio.html">CCTV on hire</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">AMC Service</a></li>
                <li><a href="practice-2.html">Data Recovery</a></li>
                <li><a href="practice-3.html">Computer Repair Services</a></li>
                <li><a href="practice-4.html">Resistant Engineer</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Solutions</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">Data Backup and Storage</a></li>
                <li><a href="practice-2.html">Antivirus And Firewall</a></li>
                <li><a href="practice-3.html">Thin Client</a></li>
                <li><a href="practice-4.html">Active Passive Networking</a></li>
            </ul>
        </li>
        
        <li> <a href="contact.html">Contact Us</a> </li>
        
        </ul>
        
      </div>
      </div>
     </div>
     
	</nav><!-- end Navigation Menu -->
        
	</div>
		
	</div>
    
	</div>
    
</header>


<!-- Slider
======================================= -->  

<div class="feature_sec11">
<div class="container">

    <h1 class="white"><strong>Antivirus Dealer in Mumbai</strong></h1>

    <h3 class="white">There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form by injected humour or randomised words which looks </h3>
    
</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec1">
<div class="container">

    <div class="title11">
    	<h2>Welcome to Hem Infotech - Providing IT Services
        <em>Experience .Reliability .Ethics</em>
        <span class="line"></span></h2>
	</div>
	
    <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form by injected humour or randomised words which
looks anitime even believable. If you are going to use passage of lorem Ipsum you need to be sure there is anything works lorem ipsum as their default model text and search for loremipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
        
    <h5>Get your Free Consultation <strong class="animate" data-anim-type="fadeIn" data-anim-delay="300">+91 966 254 7607</strong> Available 24/7 </h5>
    
</div>
</div><!-- end features section 1 -->

<div class="clearfix"></div>

<div class="feature_sec2">
<div class="container">
    
    <div class="one_fourth">
    
        <i class="fa fa-university animate zoomIn" data-anim-type="zoomIn" data-anim-delay="100"></i>
        <h4>Responsive Theme</h4>
        <p class="marb4">Making this the first true generator on the Internet uses a dictionary of over models sentence structures reasonable the always free.</p>
        <a href="#" class="button7">Read More</a>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-paper-plane animate zoomIn" data-anim-type="zoomIn" data-anim-delay="200"></i>
        <h4>Diffrent Headers</h4>
        <p class="marb4">Making this the first true generator on the Internet uses a dictionary of over models sentence structures reasonable the always free.</p>
        <a href="#" class="button7">Read More</a>
    
    </div><!-- end section -->
    
    <div class="one_fourth active">
    
        <i class="fa fa-briefcase animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"></i>
        <h4>Profession &amp; Modern</h4>
        <p class="marb4">Making this the first true generator on the Internet uses a dictionary of over models sentence structures reasonable the always free.</p>
        <a href="#" class="button7">Read More</a>
    
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <i class="fa fa-gavel animate zoomIn" data-anim-type="zoomIn" data-anim-delay="400"></i>
        <h4>Well Structured</h4>
        <p class="marb4">Making this the first true generator on the Internet uses a dictionary of over models sentence structures reasonable the always free.</p>
        <a href="#" class="button7">Read More</a>
    
    </div><!-- end section -->
    
</div>
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="one_third">
    
         <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Services</strong></div>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
         <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Products</strong></div>
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Solutions</strong></div>
    
    </div><!-- end section -->
    
 <div class="clearfix margin_top6"></div>

    <div class="one_third">
    
         <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Services</strong></div>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
         <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Products</strong></div>
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <div class="imgframe3"><img src="images/site-img51.jpg" alt=""> <strong>Solutions</strong></div>
    
    </div><!-- end section -->
    
</div>
</div>

<div class="clearfix"></div>


<div class="feature_sec6">
<div class="container">
    
    <div class="title11">
        <h2 class="white">Why <strong>Choose Us</strong>
        <span class="line3"></span></h2>
    </div>
    
    <br>
    
    <div class="one_third animate fadeInLeft" data-anim-type="fadeInLeft" data-anim-delay="300">
        
        <img src="images/site-img14.jpg" alt="">
        
        <a href="#">EXPERT & PROFESSIONAL</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="300">
        
        <img src="images/site-img15.jpg" alt="">
        
        <a href="#">PROFESSIONAL APPROACH</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="300">
        
        <img src="images/site-img16.jpg" alt="">
        
        <a href="#">HIGHT QUALITY WORK</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="one_third animate fadeInLeft" data-anim-type="fadeInLeft" data-anim-delay="400">
        
        <img src="images/site-img17.jpg" alt="">
        
        <a href="#">SATISFACTION GUARANTEE</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">
        
        <img src="images/site-img18.jpg" alt="">
        
        <a href="#">QUICK IN RESPONSE</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="400">
        
        <img src="images/site-img19.jpg" alt="">
        
        <a href="#">24/7 EMERGENCY</a>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->

</div>
</div>


<div class="clearfix"></div>
<div class="feature_sec3">
<div class="container">
	
    <div class="one_half animate" data-anim-type="fadeIn" data-anim-delay="500">
    	
        <h2 class="small"><strong>FAQ</strong></h2>

        <div id="st-accordion-four" class="st-accordion-four">
        <ul>
            <li class="" style="overflow: hidden; height: 50px;">
                <a href="#">4 Diffrent Creative Header Styles<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>She packed her seven versalia, put her initial into the belt and made on the way.</p>
                    <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>

                </div>
            </li>
            <li>
                <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>

              </div>
            </li>
            <li>
                <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>O my friend - but it is too much for my strength - I sink under the weight of the splendour of these visions!</p>
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
                </div>
            </li>
            <li>
                <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?"</p>
                    <p>He thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
                </div>
            </li>
            
        </ul>
    </div>
        
    	
    </div><!-- end all section -->
    
    
    <div class="one_half last hpeosays animate" data-anim-type="fadeIn" data-anim-delay="700">
    	
        <h2 class="small">Why Clients <strong>Love Us</strong></h2>

        <div id="owl-demo3" class="owl-carousel small">
    
            <div>
            
            	<img src="images/site-img1.jpg" alt="" />
                
                <p><strong>Lorem that more</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Ricky Holness, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img3.jpg" alt="" />
                
                <p><strong>Packages that</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Jean Desmond, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img2.jpg" alt="" />
                
                <p><strong>Apposed to using</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Devin Braedon, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img4.jpg" alt="" />
                
                <p><strong>Webpage editors</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Cason Harrison, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
               
		</div>
	</div><!-- end all section -->
	
    
     
</div>
</div><!-- end features section 3 -->

<div class="clearfix"></div>



<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong>Clients</strong><span></span></span></h2>
    
    </div><!-- end section title heading -->
    
    <br>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/1.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Microsoft</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/2.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">ebay</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/3.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Adobe</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/4.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">IBM</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/5.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Google</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/17.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Yahoo</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/7.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Discovery</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/8.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">envato</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/9.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Deloitte</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/20.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Audi</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->

    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/11.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Yahoo</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/12.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Discovery</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/13.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">envato</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/14.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Deloitte</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/16.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Audi</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec9">
<div class="container transpef">
	
    <div class="title11">
    	<h2 class="white">Need Legal Assistance - Do You Have a Claim?
        <em>Feel free to get in touch with any enquiries and one of our friendly members of staff will get back to you as soon as possible.</em>
        </h2>
	</div>
    
    <div class="cforms">
        
        <form action="http://gsrthemes.com/lawswift/boxed/demo-contacts.php" method="post" id="sky-form" class="sky-form">
          <fieldset>
            <div class="row">
            
              <div class="col col-4">
                <label class="label"><strong>Your Name*</strong></label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </div>
              
              <div class="col col-4">
                <label class="label"><strong>Your E-mail*</strong></label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </div>
            
                        
            <div class="col col-4">
              <label class="label"><strong>Phone Number</strong></label>
              <label class="input"> <i class="icon-append icon-tag"></i>
                <input type="text" name="subject" id="subject">
              </label>
            </div>
            
            </div>
  			
            <br />
             
            <div>
              <label class="label"><strong>Message*</strong></label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="5" name="message" id="message"></textarea>
              </label>
            </div>
            
          </fieldset>
          <footer>
            <button type="submit" class="subbutton">Submit Your Message</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
	</div>
    
	<div class="clearfix margin_top4"></div>
    
</div>
</div><!-- end features section 9 -->

<div class="clearfix"></div>


<footer>

<div class="footer">

    <div class="container">
    
        <div class="left">
        
            <h5>Get Free Consultation</h5>
            <h6>Available 24/7</h6>
            <h3>+91 966 254 7607</h3>
            
        </div><!-- end section -->
        
        <div class="center">
        
            <h5>Message Us Now</h5>
            <h6>Available 24/7</h6>
            <h3><a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a></h3>
        
        </div><!-- end section -->
        
        <div class="right">
        
            <h5>Address Location</h5>
            <h6>315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, Ahmedabad, Gujarat 380006 <br /> <a href="#">View Map</a></h6>


            
        
        </div><!-- end section -->
    
    </div>
    
</div><!-- end footer -->

<div class="copyrights">
<div class="container">

	<div class="one_half">Copyright © 2018 Hem Infotech. All Rights Reserved</div>
	
    <div class="one_half last"><a href="#">Notices</a> | <a href="#">Privacy Policy</a> | <a href="#">Careers</a></div>
    
    
</div>
</div><!-- end copyrights -->

</footer>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>

<!-- mega menu -->
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<!-- MasterSlider -->
<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	 slider.setup('masterslider' , {
		 width:1400,    // slider standard width
		 height:580,   // slider standard height
		 space:0,
		 speed:45,
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"basic"
	});

})(jQuery);
</script>

<!-- scroll up -->
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>

<!-- forms -->
<script src="js/form/jquery.form.min.js"></script>
<script src="js/form/jquery.validate.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";

	$(function()
	{
		// Validation
		$("#sky-form").validate(
		{					
			// Rules for form validation
			rules:
			{
				name:
				{
					required: true
				},
				email:
				{
					required: true,
					email: true
				},
				message:
				{
					required: true,
					minlength: 10
				}
			},
								
			// Messages for form validation
			messages:
			{
				name:
				{
					required: 'Please enter your name',
				},
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				},
				message:
				{
					required: 'Please enter your message'
				}
			},
								
			// Ajax form submition					
			submitHandler: function(form)
			{
				$(form).ajaxSubmit(
				{
					success: function()
					{
						$("#sky-form").addClass('submited');
					}
				});
			},
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
	});			

})(jQuery);
</script>

<!-- cubeportfolio -->
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>

<!-- owl carousel -->
<script src="js/carouselowl/owl.carousel.js"></script>

<!-- tabs -->
<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<script type="text/javascript" src="js/universal/custom.js"></script>

<!-- Accordion-->
<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>


</body>

<!-- Mirrored from gsrthemes.com/lawswift/boxed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Mar 2015 05:47:53 GMT -->
</html>
