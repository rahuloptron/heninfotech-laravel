@extends('layouts.app2')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/data-backup.jpg);">
<div class="container">

    <h1 class="white"><strong>Data Backup Solutions</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>BEST DATA BACKUP SOLUTIONS & RECOVERY SERVICES IN GUJRAT</h4>
    
    <p class="big_text3">We provide Data Backup & Recovery Solutions to maintain your critical computer problems. A wide range of Backup, Disaster and Recovery Solutions are offered to fit all budgets. When you lose data it is essential that you restore it and restore it on time. It is necessary for all individuals and businesses to understand its importance and implement data back- up plan.</p>
    <div class="margin_top1"></div>
     <p class="big_text3">We offer simple and automotive Data Backup & Recovery  Services in Vile Parle & Mumbai. Our experienced team will first analyse the problem, understand the client’s need, size and type of data and lastly suggest the effective Business Data Backup Solution.</p>
    
</div>

<div class="clearfix"></div>

<div class="feature_sec6">
<div class="container">
    
    <div class="title11">
        <h2>Our Backup system provides the following 
        <span class="line3"></span></h2>
    </div>
    
    <br>
    
    <div class="one_third">
        
       
        
        <h4>Automated incremental block heel</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third">
        
      
        
        <h4>Intelligent de-duplication backup</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last">
        
      
        
        <h4>Database Backup and Restore</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="one_third">
        
      
        
        <h4>Ensure backup compliance with email alerts</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third">
        
      
        
        <h4>Inbuilt integrated secure cloud backup</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last">
        
      
        
        <h4>Online data backup services</h4>

        <p>Model text and a search for  lorem ipsum cover many web sites.</p>
         
    </div><!-- end section -->

</div>
</div>


<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    <div class="title11">
    
        <h2>Key Benefits:


        <span class="line2"></span></h2>
        
    </div>
    
    <br>
    
    <div>
    
            <div>
            
                <div class="one_half"><img src="../images/site-img29.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    
                <ul class="list9">
                <li><i class="fa fa-long-arrow-right"></i> Completely human independent</li>

                <li><i class="fa fa-long-arrow-right"></i> Automated multiple backup scheduling</li>
                <li><i class="fa fa-long-arrow-right"></i> Takes full as well as incremental backup</li>
                <li><i class="fa fa-long-arrow-right"></i> Needs minimum monitoring</li>
                <li><i class="fa fa-long-arrow-right"></i> Backup Network & NAS drives</li>
                <li><i class="fa fa-long-arrow-right"></i> Backup open file streamlined</li>

                 <li><i class="fa fa-long-arrow-right"></i> Data backup and quick and reliable restores</li>

                  <li><i class="fa fa-long-arrow-right"></i> Storage capacities can be increased as and when required</li>

                   <li><i class="fa fa-long-arrow-right"></i> Secure online data storage</li>
                
                    </ul>
                
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            
        </div>

</div>
</div>

<div class="clearfix"></div>


<div class="feature_sec13">
<div class="container">
    
    <div class="one_half">
    
        <h3>People <strong>Say’s</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- EAGLE CORPORATION</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- NIGAM AMIN, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- R. R. KAPADIA</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- J.M.CHAUHAN, THE BAR COUNCIL OF GUJARAT</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- CONFEDERATION OF INDIAN INDUSTRY (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- C.P. MATHUR, EMPLOYEES PROVIDENT FUND ORGANISATION</strong></div>
                   
            </div><!-- end section -->
            
        </div>
        
    </div><!-- end all section -->
    
    
    <div class="one_half last">
    
        <h3><strong>FAQ</strong></h3>
        
        <div id="st-accordion-four" class="st-accordion-five">
        <ul>
            <li>
                <a href="#"> How does In-lab Data Recovery pricing work?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>You pay a flat engagement fee for every recovery case (for one or multiple drives in case of RAID) you submit at the time of case submission and it covers the cost of initial evaluation. Two-way shipping is complimentary in most locations globally. Once data has been successfully recovered, your payment method on file will be charged for the cost of data recovery. To make the entire process transparent, the total cost is clearly stated even before you submit the case and we will never charge anything extra.</p>
                </div>
            </li>
            <li>
                <a href="#">Which areas do you service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>We service entire mumbai municipal area and suburbs with onsite "Hands & Feet" services. Other locations are serviced through remote services</p>
                </div>
            </li>
            <li>
                <a href="#">Do you take AMC of corporate PCs?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>A - Yes we take AMC of corporate PCs.</p>
                </div>
            </li>
            <li>
                <a href="#">Is there a minimum quantity of computers required to avail ?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Yes, you need to have a minimum of 10 computers to avail our AMC Services.</p>
                </div>
            </li>
             <li>
                <a href="#">What is your response time for AMC services?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Service request booked before 1 PM are serviced same day. Those booked after1 PM are serviced next day. If multiple PCs are affected, services are provided within 4 Hrs.</p>
                </div>
            </li>
            
        </ul>
        </div>

    </div><!-- end all section -->
    
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>

<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="../images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>
<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    


@stop
