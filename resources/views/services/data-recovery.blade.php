@extends('layouts.app2')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/data-recovery.jpg);">
<div class="container">

    <h1 class="white"><strong>Data Recovery Services</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>Hard Drive Failed? Pen drive not Working? Data lost?No backup? <span class="color">We Can Help....</span></h4>
    
    <p class="big_text3">Getting Your data back is as easy as 123... Now we at The Cybertech make it easy for you. with cutting edge Technology,Tools and advanced proprietary software,our highly skilled professional data recovery specialists can retrieve your valuable data.</p>
    
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">

    <div class="one_full title8">
    
        <h2><span class="line"></span><span class="text"> <strong>Solutions</strong></span></h2>
        
        
    </div>

     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
      <div class="imgframe3"><img src="../images/hard-disk.jpg" alt=""><strong>HARD DRIVE
</strong> </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
         <div class="imgframe3"><img src="../images/raid-recovery.jpg" alt=""> <strong>RAID</strong></div>
    
    </div><!-- end section -->

    <div class="one_fourth">
    
         <div class="imgframe3"><img src="../images/usb2_sticks.jpg" alt=""> <strong>FLASH DRIVE</strong></div>
    
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <div class="imgframe3"><img src="../images/cell-phone.jpg" alt=""> <strong>CELL PHONE</strong></div>
    
    </div><!-- end section -->

    
</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    <div class="title11">
    
        <h2>Data recovery Features 
        <span class="line2"></span></h2>
        
    </div>
    
    <br>
    
    <div>
    
            <div>
            
                <div class="one_half"><img src="../images/hard-disk-repair.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    
                    <ul class="list9">
                <li><i class="fa fa-long-arrow-right"></i> Free diagnostics</li>

                <li><i class="fa fa-long-arrow-right"></i> No data  - No charge</li>
                <li><i class="fa fa-long-arrow-right"></i> All types of devices recovered from</li>
                <li><i class="fa fa-long-arrow-right"></i> Self-service to full account management</li>
                <li><i class="fa fa-long-arrow-right"></i> Best-in-class data recovery services</li>
                <li><i class="fa fa-long-arrow-right"></i> 100% data confidentiality.</li>
                <li><i class="fa fa-long-arrow-right"></i> Solutions from 12 hours to 30 days</li>
                <li><i class="fa fa-long-arrow-right"></i> Full support before and after your recovery procedures</li>
                    </ul>
                
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            
        </div>

</div>
</div>

<div class="clearfix"></div>


<div class="feature_sec13">
<div class="container">
    
     <div class="one_half">
    
        <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- EAGLE CORPORATION</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- NIGAM AMIN, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- R. R. KAPADIA</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- J.M.CHAUHAN, THE BAR COUNCIL OF GUJARAT</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- CONFEDERATION OF INDIAN INDUSTRY (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- C.P. MATHUR, EMPLOYEES PROVIDENT FUND ORGANISATION</strong></div>
                   
            </div><!-- end section -->
            
        </div>
        
    </div><!-- end all section -->
    
    
    <div class="one_half last">
    
        <h3><strong>FAQ</strong></h3>
        
        <div id="st-accordion-four" class="st-accordion-five">
        <ul>
            <li>
                <a href="#"> How does In-lab Data Recovery pricing work?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>You pay a flat engagement fee for every recovery case (for one or multiple drives in case of RAID) you submit at the time of case submission and it covers the cost of initial evaluation. Two-way shipping is complimentary in most locations globally. Once data has been successfully recovered, your payment method on file will be charged for the cost of data recovery. To make the entire process transparent, the total cost is clearly stated even before you submit the case and we will never charge anything extra.</p>
                </div>
            </li>
            <li>
                <a href="#">Which areas do you service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>We service entire mumbai municipal area and suburbs with onsite "Hands & Feet" services. Other locations are serviced through remote services</p>
                </div>
            </li>
            <li>
                <a href="#">Do you take AMC of corporate PCs?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>A - Yes we take AMC of corporate PCs.</p>
                </div>
            </li>
            <li>
                <a href="#">Is there a minimum quantity of computers required to avail ?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Yes, you need to have a minimum of 10 computers to avail our AMC Services.</p>
                </div>
            </li>
             <li>
                <a href="#">What is your response time for AMC services?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Service request booked before 1 PM are serviced same day. Those booked after1 PM are serviced next day. If multiple PCs are affected, services are provided within 4 Hrs.</p>
                </div>
            </li>
            
        </ul>
        </div>

    </div><!-- end all section -->
    
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>

<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="../images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>



<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    

@stop
