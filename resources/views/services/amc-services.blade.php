@extends('layouts.app2')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/amc.png);">
<div class="container">

    <h1 class="white"><strong>AMC Services in Gujrat</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>HEM INFOTECH BEST COMPUTER AMC SERVICES IN GUJRAT</h4>
    
    <p class="big_text3">Preventive maintenance is needed to ensure reliability and smooth functioning of your computer and other IT products and services. We at Micronic offers a complete peace of mind for all IT maintenance related matters.</p>

    <div class="margin_top2"></div>

     <p class="big_text3">Our Computer Annual maintenance contract services include AMC laptop, AMC computer, AMC server, AMC printers and Many more. Products under the service contract are proactively monitored to identify the issue and resolve beforehand. We believe in customer satisfaction, quick & lowest downtime and offer best price. We address the AMC for software’s service providers, dealers and vendors Mumbai.</p>
    
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <ul class="pop-wrapper">
    
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="200"><a href="#"> <img src="../images/icon-22.png" alt=""> <h6>Quality Service</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"> <img src="../images/icon-7.png" alt=""> <h6>Expert Engineers</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="400"><a href="#"> <img src="../images/icon-19.png" alt=""> <h6>Fair Pricing</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="500"><a href="#"> <img src="../images/icon-16.png" alt=""> <h6>Quick Delivery</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="600"><a href="#"> <img src="../images/icon-17.png" alt=""> <h6>Incredible Features</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="700"><a href="#"> <img src="../images/icon-25.png" alt=""> <h6>Free Updates Lifetime</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
    </ul>
    
  
</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    <div class="title11">
    
        <h2>AMC Services <strong> Offered </strong>
        <span class="line2"></span></h2>
        
    </div>
    
    <br />
    
    <div >
    
            <div>
            
                <div class="one_half"><img src="../images/amc-repair-services.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                    
                    <ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i>Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network & Virus Protection Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Printer and Network printer Management Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Data Backup & Recovery Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Software related Services</li>
                    </ul>
                
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            
        </div>

</div>
</div><!-- end features section 10 -->

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="counters1">
    
         <div class="one_fourth"> <span id="target5">0</span><span>+</span> <h4>Years</h4> </div>

        <div class="one_fourth"> <span id="target">0</span><span>+</span> <h4>IT Projects</h4> </div>
        
        <div class="one_fourth"> <span id="target2">0</span><span>+</span> <h4>Clients Served</h4> </div>
        
        <div class="one_fourth last"> <span id="target3">0</span><span>k+</span> <h4>IT Solutions Delivered</h4> </div>
        
       
        
    </div><!-- end counters1 section -->
    
    
</div>
</div><!-- end content area -->

<div class="clearfix"></div>


<div class="feature_sec13">
<div class="container">
    
    <div class="one_half">
    
        <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- EAGLE CORPORATION</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- NIGAM AMIN, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- R. R. KAPADIA</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- J.M.CHAUHAN, THE BAR COUNCIL OF GUJARAT</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- CONFEDERATION OF INDIAN INDUSTRY (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- C.P. MATHUR, EMPLOYEES PROVIDENT FUND ORGANISATION</strong></div>
                   
            </div><!-- end section -->
            
        </div>
        
    </div><!-- end all section -->
    
    
    <div class="one_half last">
    
        <h3><strong>FAQ</strong></h3>
        
        <div id="st-accordion-four" class="st-accordion-five">
        <ul>
            <li>
                <a href="#"> How much you charge for Computer AMC Service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Our Computer AMC Charges are Rs.2300/ and upwards Per PC Per Annum depending upon complexity, criticality and applications used.</p>
                </div>
            </li>
            <li>
                <a href="#">Which areas do you service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>We service entire mumbai municipal area and suburbs with onsite "Hands & Feet" services. Other locations are serviced through remote services</p>
                </div>
            </li>
            <li>
                <a href="#">Do you take AMC of corporate PCs?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>A - Yes we take AMC of corporate PCs.</p>
                </div>
            </li>
            <li>
                <a href="#">Is there a minimum quantity of computers required to avail ?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Yes, you need to have a minimum of 10 computers to avail our AMC Services.</p>
                </div>
            </li>
             <li>
                <a href="#">What is your response time for AMC services?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Service request booked before 1 PM are serviced same day. Those booked after1 PM are serviced next day. If multiple PCs are affected, services are provided within 4 Hrs.</p>
                </div>
            </li>
            
        </ul>
        </div>

    </div><!-- end all section -->
    
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>
<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="../images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>

<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    

@stop
