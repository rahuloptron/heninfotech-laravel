@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/computer-repair.jpg);">
<div class="container">

    <h1 class="white"><strong>Computer Repair Services</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>HEM INFOTECH BEST COMPUTER REPAIR, UPGRADE AND MAINTENANCE SERVICES</h4>
    
    <p class="big_text3">HEM INFOTECH is a one stop solution provider for all computer repair issues. Whether your computer is infected by a virus or it needs a hardware upgrade, we can help to fix any computer problem.</p>
 <div class="margin_top2"></div>
    <p class="big_text3">Micronic delivers on-demand, onsite computer and laptop repair services in Vile Parle & Mumbai, that are convenient and reliable. Whether it is to improve your computer’s performance with an upgrade, get those annoying ad-ware pop-ups under control or just learn how to use your pc more effectively.</p>



    
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <ul class="pop-wrapper">
    
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="200"><a href="#"> <img src="images/icon-22.png" alt=""> <h6>Quality Service</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"> <img src="images/icon-7.png" alt=""> <h6>Expert Engineers</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="400"><a href="#"> <img src="images/icon-19.png" alt=""> <h6>Fair Pricing</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="500"><a href="#"> <img src="images/icon-16.png" alt=""> <h6>Quick Delivery</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="600"><a href="#"> <img src="images/icon-17.png" alt=""> <h6>Incredible Features</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="700"><a href="#"> <img src="images/icon-25.png" alt=""> <h6>Free Updates Lifetime</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
    </ul>
    
  
</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec12">
<div class="container">
    
    <div class="title11">
        <h2>Key <strong>Features</strong>
        <span class="line4"></span></h2>
    </div>
    
    <br>
    
  
    <div class="one_fourth">
    
        <i class="fa fa-paper-plane-o"></i>
        
        <h5>Hadware Replacement</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-laptop"></i>
        
        <h5>Software Installation</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-thumbs-o-up"></i>
        
        <h5>Formatting and Antivirus</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <i class="fa fa-bell-o"></i>
        
        <h5>Hardware Upgradation</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="clearfix margin_top6"></div>
   
    
    <div class="one_fourth">
    
        <i class="fa fa-star-o"></i>
        
        <h5>On The Service</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-heart-o"></i>
        
        <h5>Quality Service</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-bookmark-o"></i>
        
        <h5>Dedicated Customer Support</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <i class="fa fa-envelope-o"></i>
        
        <h5>On The Service</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="counters1">
    
         <div class="one_fourth"> <span id="target5">0</span><span>+</span> <h4>Years</h4> </div>

        <div class="one_fourth"> <span id="target">0</span><span>+</span> <h4>IT Projects</h4> </div>
        
        <div class="one_fourth"> <span id="target2">0</span><span>+</span> <h4>Clients Served</h4> </div>
        
        <div class="one_fourth last"> <span id="target3">0</span><span>k+</span> <h4>IT Solutions Delivered</h4> </div>
        
       
        
    </div><!-- end counters1 section -->
    
    
</div>
</div><!-- end content area -->

<div class="clearfix"></div>

<div class="feature_sec6">
<div class="container">
    
    <div class="title11">
        <h2> <strong> What We Do?</strong>
        <span class="line3"></span></h2>
    </div>
    
    <br>
    
    <div class="one_third animate fadeInLeft" data-anim-type="fadeInLeft" data-anim-delay="300">
        

        <h4>Formatting and Anti Virus</h4>

        <p>Formatting of your machine with antivirus and data restoration.</p>
         
    </div><!-- end section -->
    
    <div class="one_third animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="300">
        
     
        
        <h4>Hard drive</h4>

        <p> Avail this service for hardware issues such as blue screens,floating pixels etc.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="300">
        
    
        
        <h4>Data Recovery</h4>

        <p>We can recover all your valubale data and also get hard disk repaired.</p>
         
    </div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="one_third animate fadeInLeft" data-anim-type="fadeInLeft" data-anim-delay="400">
        
   
        
        <h4>Software Installation(OS)</h4>

        <p>All type of Software/OS installations and fixes are provided.</p>
         
    </div><!-- end section -->
    
    <div class="one_third animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">
        
      
        
        <h4>Speaker/Mic</h4>

        <p>We repair and replace all kinds of speakers or mic .</p>
         
    </div><!-- end section -->
    
    <div class="one_third last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="400">
        
      
        
        <h4>Display Problems</h4>

        <p>We will solve all display issues such as a distorted/no display and so on.</p>
         
    </div><!-- end section -->

    <div class="clearfix margin_top5"></div>
    
    <div class="one_third animate fadeInLeft" data-anim-type="fadeInLeft" data-anim-delay="400">
        
   
        
        <h4>Screens Repairs</h4>

        <p>All type of Software/OS installations and fixes are provided.</p>
         
    </div><!-- end section -->
    
    <div class="one_third animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">
        
      
        
        <h4>Keyboard / Mouse / Trackpad</h4>

        <p>Keys on the keyboard / trackpad / touchpad isn’t responding we can get it repaired or replaced.</p>
         
    </div><!-- end section -->
    
    <div class="one_third last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="400">
        
      
        
        <h4>Battery</h4>

        <p>laptop is getting drained way too fast, We can repair/replace the battery of your laptop.</p>
         
    </div><!-- end section -->


</div>
</div>

<div class="clearfix"></div>



<div class="feature_sec13">
<div class="container">
    
   <div class="one_half">
    
       <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- EAGLE CORPORATION</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- NIGAM AMIN, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- R. R. KAPADIA</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- J.M.CHAUHAN, THE BAR COUNCIL OF GUJARAT</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- CONFEDERATION OF INDIAN INDUSTRY (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- C.P. MATHUR, EMPLOYEES PROVIDENT FUND ORGANISATION</strong></div>
                   
            </div><!-- end section -->
            
        </div>
        
    </div><!-- end all section -->
    
    
    <div class="one_half last">
    
        <h3><strong>FAQ</strong></h3>
        
        <div id="st-accordion-four" class="st-accordion-five">
        <ul>
            <li>
                <a href="#"> How much you charge for Computer AMC Service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Our Computer AMC Charges are Rs.2300/ and upwards Per PC Per Annum depending upon complexity, criticality and applications used.</p>
                </div>
            </li>
            <li>
                <a href="#">Which areas do you service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>We service entire mumbai municipal area and suburbs with onsite "Hands & Feet" services. Other locations are serviced through remote services</p>
                </div>
            </li>
            <li>
                <a href="#">Do you take AMC of corporate PCs?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>A - Yes we take AMC of corporate PCs.</p>
                </div>
            </li>
            <li>
                <a href="#">Is there a minimum quantity of computers required to avail ?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Yes, you need to have a minimum of 10 computers to avail our AMC Services.</p>
                </div>
            </li>
             <li>
                <a href="#">What is your response time for AMC services?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>Service request booked before 1 PM are serviced same day. Those booked after1 PM are serviced next day. If multiple PCs are affected, services are provided within 4 Hrs.</p>
                </div>
            </li>
            
        </ul>
        </div>

    </div><!-- end all section -->
    
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>
<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-institude-gandhinagar.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/employees-provident.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ca-logo.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gujarat-university.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gandhi-bakery.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="images/clients/sunheart.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ramdev-since.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/chocolate-room.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gsec.jpg" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/confederation-indian-industry.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/client_saffrony.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gujarat-state-bar-council.jpg" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/1.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>

<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>



<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>



@stop
