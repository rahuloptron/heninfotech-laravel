@extends('layouts.apps')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/parallax6.jpg);">
<!--   <div class="feature_sec11" style="background-image: url(../images/parallax_bg15.jpg)"> -->
<div class="container">

    <h1 class="white"><strong>Contact Us</strong></h1>
    
</div>
</div>
<div class="clearfix"></div>

<div class="content_fullwidth">

<div class="container">
	
    <div class="one_half">
      
        <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
        <br />
        <p>Please be patient while waiting for response. (Available 10:00 AM - 07:00 PM Support!) <strong>Phone General Inquiries: +91 966 254 7607</strong></p>
        <br /><br />
        
        <div class="cforms">
        
        {!! Form::open(['url' => 'send', 'method' => 'POST', 'class' => 'sky-form', 'files' => true]) !!}


       
          <header>Contact <strong>Form</strong></header>
          <fieldset>
            <div class="row">



              <section class="col col-6">
                <label class="label">Name</label>
                 <label class="input"> 
               <input id="name" type="text" name="name" value="{{ old('name') }}" autofocus>
              @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif

              </label>
              </section>
               <section class="col col-6">
                <label class="label">E-mail</label>
                <label class="input"> 
            
                  <input type="email" name="email" id="email" value="{{ old('email') }}" autofocus>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </label>
              </section>
            </div>
             <div class="row">
              <section class="col col-6">
                <label class="label">Mobile</label>
                <label class="input"> 
                  <input type="text" name="mobile" id="mobile" value="{{ old('name') }}" autofocus>
                  @if ($errors->has('mobile'))
                      <span class="help-block">
                          <strong>{{ $errors->first('mobile') }}</strong>
                      </span>
                  @endif
                </label>
              </section>
              <section class="col col-6">
                <label class="label">Select Service</label>
                <label class="select"><i class="fa fa-sort-desc"></i>
                  <select name="service"> 
                    <option value="">Select</option>
                    <option value="AMC and Repair Service">AMC and Repair Service</option>
                    <option value="Data Recovery">Data Recovery</option>
                    <option value="Antivirus And Firewall">Antivirus And Firewall</option>
                    <option value="Data Backup and Storage">Data Backup and Storage</option>
                    <option value="Thin Client">Thin Client</option>

                  </select>
                </label>
              </section>
            </div>
            <section>
              <label class="label">Message</label>
              <label class="textarea"> 
                <textarea rows="4" name="message1" id="message1"></textarea>
              </label>
            </section>

        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                          
          <div class="col-md-6 pull-center">
              {!! app('captcha')->display() !!}

          @if ($errors->has('g-recaptcha-response'))
              <span class="help-block">
                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
              </span>
          @endif
          </div>
           </div>
        

          </fieldset>
          <footer>
            <button type="submit" class="button">Send message</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>

           

       {!! Form::close() !!}
        
        </div>
        
      </div>
      
      <div class="one_half last">
      
        <div class="address_info">
        
          <h4>Company <strong>Address</strong></h4>
          <ul>
            <li> <strong>Hem Infotech</strong><br />
              315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, <br />Ahmedabad, Gujarat 380006<br />
              Mobile: +91 966 254 7607<br />
              E-mail: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a><br />
            
          </ul>
        </div>
        <div class="clearfix"></div>
        <h4>Find the <strong>Address</strong></h4>
       
        <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.984727120046!2d72.56857631454594!3d23.024332984952142!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e8456f07d5a17%3A0x12f6efc4106a2c9a!2sHem+Infotech+%7C+Leading+IT+Services%2C+CRM+Solutions+Provider!5e0!3m2!1sen!2sin!4v1520504067720" width="546" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>
        <br />
        <small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=WA,+United+States&amp;aq=0&amp;oq=WA&amp;sll=47.605288,-122.329296&amp;sspn=0.008999,0.016544&amp;ie=UTF8&amp;hq=&amp;hnear=Washington,+District+of+Columbia&amp;t=m&amp;z=7&amp;iwloc=A">View Larger Map</a></small> </div>

</div>
</div><!-- end content area -->




<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


</div>
</div>


@stop