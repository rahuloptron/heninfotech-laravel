@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/Parallax1.jpg);">
<div class="container">

    <h1 class="white"><strong>BUILD THE
FUTURE</strong></h1>
   <div class="margin_top3"></div>
   <a href="#" class="but_medium2"><i class="fa fa-check-square-o"></i>&nbsp; Apply Now</a>
    
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

	<div class="one_half">
	
    <h3>Who is HemInfotech?</h3>

    <p class="big_text3">Join a digital services leader that will engage your mind, inspire your creativity and develop your career.</p>

        <div class="margin_top1"></div>
        
    <p class="big_text3">We are a leading international IT Solutions company with a client base of international blue-chip companies across all industry sectors.</p>

    <div class="margin_top1"></div>

    <p class="big_text3">HemInfotech is focused on business technology that powers progress and helps organizations to create their firm of the future.</p>

    <div class="margin_top1"></div>

    <p class="big_text3">Our ability to deliver Operational Excellence means that we are always ready and able to help our clients address whatever challenges they face, wherever they are in the world. </p>

 

    </div>

    <div class="one_half last">
    	
    	<img src="images/company-profile.jpg" alt="">

    </div>

    <div class="divider_line10 margin_top3"></div>

    <div class="content">
    	
    	<h3>Current Openings:</h3>

    </div>




</div><!-- end section -->



<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop
