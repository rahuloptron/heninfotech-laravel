@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/parallax-process.jpg);">
<div class="container">

    <h1 class="white"><strong>Privacy Policy</strong></h1>
    
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

        <h4>HEM INFOTECH PRIVACY POLICY</h4>

        <p class="big_text3">The terms “We” / “Us” / “Our”/”Company” individually and collectively refer to Hem Infotech and the terms “You” /”Your” / “Yourself” refer to the users.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">This Privacy Policy is an electronic record in the form of an electronic contract formed under the information Technology Act, 2000 and the rules made thereunder and the amended provisions pertaining to electronic documents / records in various statutes as amended by the information Technology Act, 2000. This Privacy Policy does not require any physical, electronic or digital signature.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">This Privacy Policy is a legally binding document between you and Hem Infotech (both terms defined below). The terms of this Privacy Policy will be effective upon your acceptance of the same (directly or indirectly in electronic form, by clicking on the I accept tab or by use of the website or by other means) and will govern the relationship between you and Hem Infotech for your use of the website www.heminfotech.com (defined below).</p>
        <div class="margin_top2"></div>

        <p class="big_text3">This document is published and shall be construed in accordance with the provisions of the Information Technology (reasonable security practices and procedures and sensitive personal data of information) rules, 2011 under Information Technology Act, 2000; that require publishing of the Privacy Policy for collection, use, storage and transfer of sensitive personal data or information.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">Please read this Privacy Policy carefully by using the Website, you indicate that you understand, agree and consent to this Privacy Policy. If you do not agree with the terms of this Privacy Policy, please do not use this Website.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by us as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.</p>
        <div class="margin_top3"></div>

        <h4>User Information</h4>

        <p class="big_text3">To avail certain services on our Websites, users are required to provide certain information for the registration process/form filling namely: – a) your name, b) email address, c) sex, d) age, e) PIN code and / or your occupation, interests, and the like. The Information as supplied by the users enables us to improve our sites and provide you the most user-friendly experience.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">All required information is service dependent and we may use the above said user information to, maintain, protect, and improve its services (including advertising services) and for developing new services.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">Such information will not be considered as sensitive if it is freely available and accessible in the public domain or is furnished under the Right to Information Act, 2005 or any other law for the time being in force.</p>

         <div class="margin_top3"></div>

        <h4>Cookies</h4>

        <p class="big_text3">To improve the responsiveness of the sites for our users, we may use “cookies”, or similar electronic tools to collect information to assign each visitor a unique, random number as a User Identification (User ID) to understand the user’s individual interests using the Identified Computer. Unless you voluntarily identify yourself (through registration, for example), we will have no way of knowing who you are, even if we assign a cookie to your computer. The only personal information a cookie can contain is information you supply (an example of this is when you ask for our Personalised Horoscope). A cookie cannot read data off your hard drive. Our advertisers may also assign their own cookies to your browser (if you click on their ads), a process that we do not control.</p> 
        <div class="margin_top2"></div>

        <p class="big_text3">Our web servers automatically collect limited information about your computer’s connection to the Internet, including your IP address, when you visit our site. (Your IP address is a number that lets computers attached to the Internet know where to send you data — such as the web pages you view.) Your IP address does not identify you personally. We use this information to deliver our web pages to you upon request, to tailor our site to the interests of our users, to measure traffic within our site and let advertisers know the geographic locations from where our visitors come.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">It’s also important to note that we allow third party behavioural tracking. We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions, and other ad service functions as they relate to our website. If you want to know more about how Google uses data, check out <a href="https://www.google.com/policies/privacy/partners/">Google privacy and terms</a>. To opt out of Google Analytics, <a href="https://tools.google.com/dlpage/gaoptout">click here.</a></p>
        <div class="margin_top3"></div>

         <h4>Links to the Other Sites</h4>

        <p class="big_text3">Our policy discloses the privacy practices for our own web site only. Our site provides links to other websites also that are beyond our control. We shall in no way be responsible in way for your use of such sites.</p>
        <div class="margin_top3"></div>

        <h4>Information Sharing</h4>

        <p class="big_text3">We shares the sensitive personal information to any third party without obtaining the prior consent of the user in the following limited circumstances:</p>
        <div class="margin_top2"></div>
        <p class="big_text3">(a) When it is requested or required by law or by any court or governmental agency or authority to disclose, for the purpose of verification of identity, or for the prevention, detection, investigation including cyber incidents, or for prosecution and punishment of offences. These disclosures are made in good faith and belief that such disclosure is reasonably necessary for enforcing these Terms; for complying with the applicable laws and regulations.</p> 
        <div class="margin_top2"></div>
        <p class="big_text3">(b) We proposes to share such information within its group companies and officers and employees of such group companies for the purpose of processing personal information on its behalf. We also ensure that these recipients of such information agree to process such information based on our instructions and in compliance with this Privacy Policy and any other appropriate confidentiality and security measures.</p>
        <div class="margin_top3"></div>

        <h4>Information Security</h4>

        <p class="big_text3">We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where we store personal data.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">All information gathered on our Website is securely stored within our controlled database. The database is stored on servers secured behind a firewall; access to the servers is password-protected and is strictly limited. However, as effective as our security measures are, no security system is impenetrable. We cannot guarantee the security of our database, nor can we guarantee that information you supply will not be intercepted while being transmitted to us over the Internet. And, of course, any information you include in a posting to the discussion areas is available to anyone with Internet access.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">However the internet is an ever evolving medium. We may change our Privacy Policy from time to time to incorporate necessary future changes. Of course, our use of any information we gather will always be consistent with the policy under which the information was collected, regardless of what the new policy may be.</p>
        <div class="margin_top3"></div>

        <h4>Grievance Redressal</h4>
        <p class="big_text3">Redressal Mechanism: Any complaints, abuse or concerns with regards to content and or comment or breach of these terms shall be immediately informed to the designated Grievance Officer as mentioned below via in writing or through email signed with the electronic signature to <a href="mailto:purav@heminfotech.com">sales@heminfotech.com</a> (Mr. Purav).</p>
        <div class="margin_top2"></div>
        <p class="big_text3">If there are any questions regarding this privacy policy, want a copy of the information we hold about you, to remove and/or correct your personal information from our database, or want to make a complaint, please contact us at <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>.</p>
        <div class="margin_top2"></div>
        <p class="big_text3">We are not responsible for communicating notifications regarding changes to our privacy policy. Any updates or changes will be posted here, however, it is your responsibility to check back on a regular basis to stay up-to-date regarding changes.</p>
        <div class="margin_top3"></div>
    
</div><!-- end section -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop
