@extends('layouts.app2')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/parallax-process.jpg);">
<div class="container">

    <h1 class="white"><strong>Network Attached Storage Solution</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>Network Attached storage Solution & Services </h4>
    
    <p class="big_text3">UTM Firewall refers to a comprehensive security product that includes protection against the multiple threats, secure individual PC’s or businesses from intruders, hackers and malicious code. A UTM appliance typically includes Firewall softwares, Virtual Private Network, Gateway Antivirus, Anti- Spam, Web Content Filtering, Antivirus firewall,  Intrusion Detection and also Centralised Management, Monitoring Logging in a single logging package.</p>
    <div class="margin_top1"></div>
     <p class="big_text3">We offer a flexible security solutions and best firewalls in Vile Parle & Mumbai to address your business requirements ranging from the network perimeter to data centre. Our team of expert comprehends the challenges and provide a security and best firewall protectionsolution that aligns with your core business objectives.</p>
    
</div>

<div class="clearfix"></div>

<div class="feature_sec12">
<div class="container">
    
    <div class="title11">
        <h2>Key <strong>Benefits</strong>
        <span class="line4"></span></h2>
    </div>
    
    <br>
    
    
    
    <div class="one_third">
    
        <i class="fa fa-paper-plane-o"></i>
        
        <h5>Firewall system protects data inside the organization from being hacked by exploiting web application vulnerabilities</h5>
        
       
    </div><!-- end section -->
    
   
    
    <div class="one_third">
    
        <i class="fa fa-thumbs-o-up"></i>
        
        <h5>Content filtering- Allows blocking of non-business related web traffic including streaming media sites, downloads, instant messaging etc. In order to reduce unnecessary load on enterprise bandwidth</h5>
      
        
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <i class="fa fa-bell-o"></i>
        
        <h5>Automatic link failover-shifts network load from inactive ISP lines to active lines in order to reduce internet downtime and streamline the transition.</h5>
        
      
        
    </div><!-- end section -->
    
    <div class="clearfix margin_top6"></div>
    
   
    
    <div class="one_third">
    
        <i class="fa fa-heart-o"></i>
        
        <h5>Bandwidth Manager-allows the allocation of enterprise bandwidth on the basis of individual users or user groups.</h5>
        
   
        
    </div><!-- end section -->
    
    <div class="one_third">
    
        <i class="fa fa-bookmark-o"></i>
        
        <h5>Gateway Antivirus- Scans all incoming and outgoing network traffic at the gateway level. Extends existing virus solutions by reducing the window of vulnerability (WOV).</h5>
     
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <i class="fa fa-envelope-o"></i>
        
        <h5>VPN – provides it administrators with a means for secure communications between the company’s remote users and for building site-to-site connections</h5>
 
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>



<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop
