@extends('layouts.app2')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/antivirus.jpg);">
<div class="container">

    <h1 class="white"><strong>QUICKHEAL ANTIVIRUS DEALER</strong></h1>
    <div class="margin_top3"></div>
    <a href="#" class="but_medium2">Request Quote</a>
</div>
</div>
<div class="clearfix"></div>


<div class="container tbp">

	<h3>Looking for Quick Heal Antivirus Dealer in GUJRAT?</h3>
	
    <div class="clearfix"></div>

   	
     <p class="big_text3">HemInfotech a leading company which is highly engaged in offering professional Quick Heal Dealer in Mumbai. We have a team of highly skilled engineers, who are well-aware of modern techniques and able to complete their task within a stipulated time period. We use only cutting-edge technology in order to provide high-class services to our clients.</p>
    <div class="clearfix margin_top2"></div>
	<p class="big_text3">Our clients are happy and satisfied with our work and never get the chance to complain regarding it. Our soft-spoken team first understands the problem of a customer and then provides them the best available solution for it. We always do our best, so, that our customer will never feel regret over their decision to choose us as their service provider.</p>
    <div class="clearfix margin_top2"></div>
	<p class="big_text3">We have years of practice in this engineering work and therefore commit that here you will get the best solution for all your computer and networking related problems. If you are looking for best Quick Heal Dealer in Mumbai, so, you can contact us. We are here to offer our reasonable services to you.</p>
        <div class="clearfix margin_top2"></div>
    <p class="big_text3">Quick Heal Total Security protects your laptops and desktops against all kinds of Internet or network-based threats. Once installed, it acts as a shield against viruses, worms, Trojans, spywares and other malicious threats that can throw your system out of work. It comes with useful and user-friendly features like Quick Heal's renowned DNA Scan Technology, Anti-Phishing feature, Parental Control feature, PC Tuner tool and the PC2Mobile Scan feature to scan, detect and remove malwares from your laptops, desktops and mobile phones.</p>
    
    <div class="clearfix margin_top5"></div>
    
	<div class="one_half">
    <div class="graybgraph_box">
    	
        <h3>For Home Users</h3>
        
        <p>Protect Your PCs, Laptops, Mac, & Smartphones with Real-time Security against 
Web Based Attacks.</p>

	</div>
    </div><!-- end section -->
    
    <div class="one_half last">
    <div class="graybgraph_box">
    	
        <h3>For Enterprise</h3>
        
         <p>With our continuous innovation and simplicity, we are redefining Enterprise IT Security. Introducing Seqrite, our Enterprise Security portfolio's new identity.</p>

	</div>
    </div><!-- end section -->


</div>

<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    <div>

    	<div class="title11">
    
        <h2><strong> Features </strong>
        <span class="line2"></span></h2>
        
    	</div>
    
    <br />
    
            <div>
            
                <div class="one_half"><img src="../images/Quick_Heal_logo.png" alt="" /></div>
                
                <div class="one_half last">

                    <ul class="list9">
                        <div class="one_half">

                    <li><i class="fa fa-long-arrow-right"></i> Ransomware Protection</li>
                    <li><i class="fa fa-long-arrow-right"></i> Core Protection</li>
                    <li><i class="fa fa-long-arrow-right"></i> Enhanced Self-Protection</li>
                    <li><i class="fa fa-long-arrow-right"></i> Web Security</li>
                    <li><i class="fa fa-long-arrow-right"></i> Malware Protection</li>
                    <li><i class="fa fa-long-arrow-right"></i> Silent Mode</li>

                     <li><i class="fa fa-long-arrow-right"></i> PC2Mobile Scan</li>


                     <li><i class="fa fa-long-arrow-right"></i> TrackMyLaptop</li>
                     <li><i class="fa fa-long-arrow-right"></i> PCTuner</li>
                       
                     </div>

                        <div class="one_half last">
                        	
                    <li><i class="fa fa-long-arrow-right"></i> Email Security</li>
                    <li><i class="fa fa-long-arrow-right"></i> Safe Banking</li>
                    <li><i class="fa fa-long-arrow-right"></i> Improved Scan Engine</li>
                    <li><i class="fa fa-long-arrow-right"></i> Parental Control</li>
                    <li><i class="fa fa-long-arrow-right"></i> Privacy Protection</li>
                    <li><i class="fa fa-long-arrow-right"></i> Firewall</li>

                    <li><i class="fa fa-long-arrow-right"></i> Flash Drive Protection</li>

                    <li><i class="fa fa-long-arrow-right"></i> Safe Mode Protection</li>

                    <li><i class="fa fa-long-arrow-right"></i> Stay Connected</li>
                       
                        </div>
                    </ul>
                
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            
        </div>

</div>
</div><!-- end features section 10 -->

<div class="clearfix"></div>

<div class="feature_sec13">
<div class="container">
    
    <div class="one_half">
    
        <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- EAGLE CORPORATION</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- NIGAM AMIN, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- R. R. KAPADIA</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- J.M.CHAUHAN, THE BAR COUNCIL OF GUJARAT</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- CONFEDERATION OF INDIAN INDUSTRY (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="../images/site-img3.jpg" alt="" /> <strong>- C.P. MATHUR, EMPLOYEES PROVIDENT FUND ORGANISATION</strong></div>
                   
            </div><!-- end section -->
            
        </div>
        
    </div><!-- end all section -->
    
    
      <div class="one_half last">
    
        <h3><strong>FAQ</strong></h3>
        
        <div id="st-accordion-four" class="st-accordion-five">
        <ul>
            <li>
                <a href="#"> Important tips before proceeding with installation?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>(1).Verify that the computer is not infected (By Emergency CD). (2).Uninstall any other anti-virus programs.
					(3).Delete all files in the Windows Temporary folder.
					(4).Check available hard disk space.
					(5).Exit all programs before starting the installation
					</p>
                </div>
            </li>
            <li>
                <a href="#">Which areas do you service?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p><strong>Using Quick Heal CD:</strong></p>
                    <div class="margin_top1"></div>
                    <p>Insert Quick Heal CD. It is a auto run CD. Click on Install Quick Heal. Quick Heal has a very simple installation procedure. While you are installing, simply read each installation screen, follow the instructions, and then click Next to continue. Default options provide an ideal trade between protection and performance.</p>
                    <div class="margin_top1"></div>
                    <p><strong>Using Quick Heal Installer Program:</strong></p>
                    <div class="margin_top1"></div>
                    <p>Double click on Quick Heal installer to proceed with installation. Rest procedure is same.</p>
                </div>
            </li>
            <li>
                <a href="#">Can I install Quick Heal on another computer?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>If you install Quick Heal on another computer, it is necessary to register your software after you install Quick Heal. You must perform the Registration procedure by providing new Serial Number. Any previously obtained Serial Number and License Keys are invalid and will not work on another computer.</p>
                    <div class="margin_top1"></div>
					<p>One Serial number can only be used for one computer.</p>
                </div>
            </li>
            <li>
                <a href="#">How do I uninstall Quick Heal?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>If due to any reason you wish to uninstall Quick Heal, follow the below given steps:</p>
                    <div class="margin_top1"></div>
                    <p><i class="fa fa-long-arrow-right"></i> Click Uninstall Quick Heal from Start-Programs-Quick Heal group.</p>
                    <p><i class="fa fa-long-arrow-right"></i> Quick Heal Uninstaller will prompt for the deletion of Reports. If you wish to reinstall Quick Heal after some time then you can uncheck Remove Report Files. Otherwise proceed by pressing OK.</p>
                    <p><i class="fa fa-long-arrow-right"></i> Uninstaller at last will prompt you to restart your system for changes to take effect</p>
                    <div class="margin_top1"></div>
                    <p>1. Before proceeding with uninstallation, please ensure that all programs are closed.</p>
                    <p>2. Uninstalling Quick Heal under Windows NT/2000/XP operating system requires administrative privilege.</p>
                </div>
            </li>
             <li>
                <a href="#">How do I transfer my registered copy to upgraded PC?<span class="st-arrow">Open or Close</span></a>
                <div class="st-content">
                    <p>In this case you need to uninstall Quick Heal from old system and install it on upgraded system. You also need to reactivate Quick Heal after installation.</p>
                </div>
            </li>
            
        </ul>
        </div>

    </div><!-- end all section -->
    
    

</div>
</div><!-- end features section 13 -->


<div class="clearfix"></div>

<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="../images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="../images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>



<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    


@stop
