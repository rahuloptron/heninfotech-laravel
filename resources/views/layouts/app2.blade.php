<!doctype html>
<html>
<head>
    @include('includes.head3')
</head>
<body>
<div class="wrapper_boxed">

<div class="site_wrapper">

    <header id="header">
        @include('includes.header')
    </header>

  
            @yield('content')


    <footer>
        @include('includes.footer')
    </footer>

</div>

</div>

@include('includes.js3')

</body>
</html>