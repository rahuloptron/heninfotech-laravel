@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/parallax-process.jpg);">
<div class="container">

    <h1 class="white"><strong>Solutions</strong></h1>
 
</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    	<div>
            <div>
            
                <div class="one_half"><img src="images/site-img29.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Antivirus And Solutions</h3>
                	
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
					<br>
					 <ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                       
                    </ul>
                    
                </div>

        </div>

<div class="divider_line1"></div>
        <div>
    
            <div>
            
                <div class="one_half">


                	<h3 class="color">Databackup And Storage</h3>
                	
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
					<br>
					<ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                       
                    </ul>
                
                </div>
                
                <div class="one_half last">
                
                   <img src="images/site-img30.jpg" alt="" />
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->


        </div>

        <div class="divider_line1"></div>

        <div>
            <div>
            
                <div class="one_half"><img src="images/site-img31.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                    
                    <h3 class="color">Networking</h3>
                	
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
					<br>
					<ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                       
                    </ul>
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->

        </div>

        <div class="divider_line1"></div>

        <div>
    
            <div>
            
                <div class="one_half">


                	 <h3 class="color">Virtualisation</h3>
                	
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
					<br>
					<ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                       
                    </ul>
                
                </div>
                
                <div class="one_half last">
                
                   <img src="images/site-img32.jpg" alt="" />
  
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->


        </div>

        <div class="divider_line1"></div>

        <div>
            <div>
            
                <div class="one_half"><img src="images/site-img33.jpg" alt="" /></div>
                
                <div class="one_half last">

                	 <h3 class="color">Thin Client</h3>
                
                    
                    <ul class="list9">
                        <li><i class="fa fa-long-arrow-right"></i> Server & WorkStation Installation & Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Mail Server Configuration and Management</li>
                        <li><i class="fa fa-long-arrow-right"></i>Hardware repairing and Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network Installation & Management Service</li>
                        <li><i class="fa fa-long-arrow-right"></i>Network & Virus Protection Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Printer and Network printer Management Services</li>
                        <li><i class="fa fa-long-arrow-right"></i>Data Backup & Recovery Services</li>
                       
                    </ul>
                
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->

        </div>

         </div>

</div>
</div><!-- end features section 10 -->

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    

@stop
