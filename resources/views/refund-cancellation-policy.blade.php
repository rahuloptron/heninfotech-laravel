@extends('layouts.app')
@section('content')

<div class="feature_sec11" style="background-image: url(../images/banner/parallax-process.jpg);">
<div class="container">

    <h1 class="white"><strong>Refund Cancellation Policy</strong></h1>
    
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">

    <h4>HEM INFOTECH REFUND CANCELLATION POLICY</h4>
    
         <p class="big_text3">Our focus is complete customer satisfaction. In the event, if you are displeased with the services provided, we will refund the money, provided the reasons are genuine and proved after investigation. Please read the fine prints of each deal before buying it, it provides all the details about the services or the product you purchase.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">In case of dissatisfaction from our services, clients have the liberty to cancel their projects and request a refund from us. Our Policy for the cancellation and refund will be as follows:</p>

        <div class="margin_top3"></div>

        <h4>Cancellation Policy</h4>

        <p class="big_text3">(1) For Cancellations please contact us via <a href="contact.html">contact us link.</a></p>
        <div class="margin_top2"></div>

        <p class="big_text3">(2) Requests received later than 7 business days prior to the end of the current service period will be treated as cancellation of services for the next service period.</p>
        <div class="margin_top3"></div>

        <h4>Refund Policy</h4>


        <p class="big_text3">(1) We will try our best to create the suitable design concepts for our clients.</p>
        <div class="margin_top2"></div>

        <p class="big_text3">(2) In case any client is not completely satisfied with our products we can provide a refund.</p>

        <div class="margin_top2"></div>

        <p class="big_text3">(3) If paid by credit card, refunds will be issued to the original credit card provided at the time of purchase and in case of payment gateway name payments refund will be made to the same account.</p>
        <div class="margin_top3"></div>

       <p class="big_text3">For any queries please contact us at <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>.</p>
    
</div><!-- end section -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop
