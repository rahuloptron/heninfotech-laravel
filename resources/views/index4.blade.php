<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Hem Infotech IT Solution Service Provider</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

    <!-- animations -->
    <link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
  
    
    <!-- mega menu -->
    <link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- MasterSlider -->
	<link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="js/masterslider/skins/default/style.css" />
    
    <!-- owl carousel -->
    <link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">
    
    <!-- forms -->
    <link rel="stylesheet" href="js/form/sky-forms.css" type="text/css" media="all">
    
    <!-- tabs -->
    <link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs2.css">

    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
    
</head>

<body>

<div class="wrapper_boxed">

<div class="site_wrapper">

<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_nav">
        <div class="container">
        	
            <div class="left">
            
            	<ul class="topsocial">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            
            </div><!-- end left links -->
            
            <div class="right">
                
                Call Us: <strong>+91 966 254 7607</strong>       Email: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>
          
        </div><!-- end right links -->
        
        </div>
        </div>
            
 	</div>
    
	</div><!-- end top navigations -->
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
	<nav class="menu_main">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
         <li class="dropdown"><a href="index.html" data-toggle="dropdown" class="dropdown-toggle">home</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="index2.html">home2</a></li>
                <li><a href="index3.html">home3</a></li>
              
            </ul>
        </li>
               
         <li><a href="about.html">About</a></li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Products</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="attorneys.html">Antivirus</a></li>
                <li><a href="attorneys-2.html">Desktops</a></li>
                <li><a href="attorneys-3.html">Server And Workstations</a></li>
                <li><a href="attorneys-4.html">Firewall</a></li>
                <li><a href="attorneys-fullbio.html">CCTV on hire</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">AMC Service</a></li>
                <li><a href="practice-2.html">Data Recovery</a></li>
                <li><a href="practice-3.html">Computer Repair Services</a></li>
                <li><a href="practice-4.html">Resistant Engineer</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Solutions</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">Data Backup and Storage</a></li>
                <li><a href="practice-2.html">Antivirus And Firewall</a></li>
                <li><a href="practice-3.html">Thin Client</a></li>
                <li><a href="practice-4.html">Active Passive Networking</a></li>
            </ul>
        </li>
        
        <li> <a href="contact.html">Contact Us</a> </li>
        
        </ul>
        
      </div>
      </div>
     </div>
     
	</nav><!-- end Navigation Menu -->
        
	</div>
		
	</div>
    
	</div>
    
</header>


<!-- Slider
======================================= -->  

<div class="mstslider">

<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

    <!-- slide -->
   	<div class="ms-slide slide-1" data-delay="7">
         
        <!-- slide background -->
        <img src="images/sliders/master/slider-1.jpg"  alt=""/>     
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:120px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:116px; top:123px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        >NEED LEGAL <br /> ASSISTANCE?</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:305px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        >Get immediate free information and advice confidential from the <br />
experienced attorneys on most common legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:455px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-2" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>         
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:30px"
            data-effect="bottom(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:116px; top:120px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Making <br /> Your <br /> Priorities <br /> Ours</div>
          
        <div class="ms-layer sldbut1"
        	style="left:120px; top:466px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-3" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>          
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:0px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1 small"
        	style="left:116px; top:113px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Get <br /> confidential <br /> legal advice</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:327px"
            data-effect="left(100)"
            data-duration="1600"
            data-delay="1100"
            data-ease="easeOutExpo"
        >Get free information &amp; advice confidential on most legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:445px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="1300"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-4" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>          
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:0px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1 smalldark"
        	style="left:116px; top:143px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Experienced <br /> Attorneys</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:305px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Get immediate free information and advice confidential from the <br />
experienced attorneys on most common legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:455px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    

</div><!-- end of masterslider -->

</div><!-- end slider -->

<div class="clearfix"></div>

<div class="feature_sec1">
<div class="container">

    <div class="title11">
    	<h2>Welcome to Hem Infotech - Providing IT Services
        <em>Experience .Reliability .Ethics</em>
        <span class="line"></span></h2>
	</div>
	
    <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form by injected humour or randomised words which
looks anitime even believable. If you are going to use passage of lorem Ipsum you need to be sure there is anything works lorem ipsum as their default model text and search for loremipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
        
    <h5>Get your Free Consultation <strong class="animate" data-anim-type="fadeIn" data-anim-delay="300">+91 966 254 7607</strong> Available 24/7 </h5>
    
</div>
</div><!-- end features section 1 -->

<div class="clearfix"></div>


<div class="content_fullwidth">
<div class="container">
    
    <ul class="pop-wrapper">
    
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="200"><a href="#"> <img src="images/icon-22.png" alt=""> <h6>QUALITY SERVICE</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"> <img src="images/icon-7.png" alt=""> <h6>EXPERT ENGINEERS</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="400"><a href="#"> <img src="images/icon-19.png" alt=""> <h6>FAIR PRICING</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="500"><a href="#"> <img src="images/icon-16.png" alt=""> <h6>QUICK DELIVERY</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="600"><a href="#"> <img src="images/icon-17.png" alt=""> <h6>Incredible Features</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
        <li class="animate zoomIn" data-anim-type="zoomIn" data-anim-delay="700"><a href="#"> <img src="images/icon-25.png" alt=""> <h6>Free Updates Lifetime</h6> <span>Lorem ipsum many web sites still in infancy versions have evolved over the years.</span></a></li>
        
    </ul>
    
  
</div>
</div>

<div class="clearfix"></div>

<div class="container tbp">
    
    <div class="clearfix"></div>

    
    <div class="one_third">
    <div class="graybgraph_box">
        
        <h3>Products</h3>
        
        <ul>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Drunk Driving Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Industrial Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Maritime and Jones Act Claims</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Nursing Home Neglect</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Products Liability</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Qui Tam and Whistleblower Actions</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Trucking Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Workplace Injuries</a></li>
        </ul>

    </div>
    </div><!-- end section -->
    
    <div class="one_third">
    <div class="graybgraph_box">
        
        <h3>Services</h3>
        
         <ul>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Drunk Driving Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Industrial Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Maritime and Jones Act Claims</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Nursing Home Neglect</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Products Liability</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Qui Tam and Whistleblower Actions</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Trucking Accidents</a></li>
            <li><i class="fa fa-arrow-circle-o-right"></i> <a href="#">Workplace Injuries</a></li>
        </ul>

    </div>
    </div><!-- end section -->
    
    <div class="one_third last">
    <div class="graybgraph_box">
        
        <h3>Solutions</h3>
        
        <ul>
            <li><i class="fa fa-arrow-circle-o-right"></i> District of Columbia</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> Massachusetts</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> California</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> Florida</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> The State Bar of Texas</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> United States Tax Court</li>
            <li><i class="fa fa-arrow-circle-o-right"></i> US Court of Federal Claims</li>
             <li><i class="fa fa-arrow-circle-o-right"></i> US Court of Federal Claims</li>
        </ul>

    </div>
    </div><!-- end all section -->
    
  

</div>

<div class="clearfix"></div>

<div class="feature_sec17">
<div class="container">
    
    <div class="one_half">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Alex Jacobson</strong></h5>
                <em>Founder &amp; Managing Partner</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-1.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Marcos Heriberto</strong></h5>
                <em>Partner</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-2.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="one_half">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Alana Desiree</strong></h5>
                <em>Counsel</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-3.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Kiley Felicity</strong></h5>
                <em>Associate</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-4.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="one_half">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Alex Jacobson</strong></h5>
                <em>Member</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-5.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            
            <div class="carea">
            
                <h5><strong>Richard Falkner</strong></h5>
                <em>Counsel</em>
                
                <p>Letraset sheets contain lorem Ipsum passages more recently with desktop publishing software like versions.</p>
                
                <a href="attorneys-fullbio.html" class="button5"><strong>Read Full BIO</strong></a>
                
            </div>
            
            <img src="images/attorney-6.jpg" alt="">
        
        </div>
    
    </div><!-- end section -->
    

    

</div>
</div>
<div class="clearfix"></div>

<div class="parallax_section1">
<div class="container">
    
    <div class="one_half">
    
        <img src="images/site-img46.png" alt="" class="rimg">
   
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div id="st-accordion-three" class="st-accordion-two">
        <ul>
            <li class="st-open" style="overflow: hidden; height: 250px;">
                <a href="#">4 Diffrent Creative Header Styles<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: block;">
                    
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there anything hidden in the middle of text.</p>
                    
                    <br>
                    <i class="fa fa-cog fati19"></i>
                    <i class="fa fa-rocket fati19"></i>
                    <i class="fa fa-globe fati19"></i>
                    <i class="fa fa-laptop fati19"></i>
                    <i class="fa fa-user fati19"></i>

                </div>
            </li>
            <li class="" style="overflow: hidden; height: 50px;">
                <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites.</p>
                    
                    <p>Established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable.</p>
                    
                </div>
            </li>
            <li class="" style="overflow: hidden; height: 50px;">
                <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
                    
                    <p>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions.</p>
                    
                </div>
            </li>
            <li class="" style="overflow: hidden; height: 50px;">
                <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
                    
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary.</p>
                    
                    <p>Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words.</p>
                    
                </div>
            </li>
            
        </ul>
    </div>
   
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>
<div class="feature_sec3">
<div class="container">
	
    <div class="one_half animate" data-anim-type="fadeIn" data-anim-delay="500">
    	
     
    
        <h3>Why Choose Us</h3>
        
        <ul class="list9">
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Morbi faucibus augue ligula interdum ipsum ullamcorpe</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Phasellus fermentum turpis vel enim porttitor sollicitudin.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Aliquam commodo nunc vel velit porttitor tempus.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Ut facilisis elit vel magna fermentum posuere.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Donec a enim at nibh bibendum scelerisque in in nisl.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Integer vulputate neque id erat sollicitudin sodales.</a></li>
            <li><i class="fa fa-long-arrow-right"></i> <a href="#">Sed pharetra lorem sed tellus suscipit tristique.</a></li>
        </ul>
    
  
        
    	
    </div><!-- end all section -->
    
    
    <div class="one_half last hpeosays animate" data-anim-type="fadeIn" data-anim-delay="700">
    	
        <h2 class="small">Why Clients <strong>Love Us</strong></h2>

        <div id="owl-demo3" class="owl-carousel small">
    
            <div>
            
            	<img src="images/site-img1.jpg" alt="" />
                
                <p><strong>Lorem that more</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Ricky Holness, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img3.jpg" alt="" />
                
                <p><strong>Packages that</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Jean Desmond, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img2.jpg" alt="" />
                
                <p><strong>Apposed to using</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Devin Braedon, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img4.jpg" alt="" />
                
                <p><strong>Webpage editors</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Cason Harrison, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
               
		</div>
	</div><!-- end all section -->
	
    
     
</div>
</div><!-- end features section 3 -->

<div class="clearfix"></div>



<div class="feature_sec19">
<div class="container">

    <ul>
        <li class="animate fadeIn" data-anim-type="fadeIn" data-anim-delay="200"><img src="images/clientlogo1-gray.png" alt=""></li>
        <li class="animate fadeIn" data-anim-type="fadeIn" data-anim-delay="300"><img src="images/clientlogo2-gray.png" alt=""></li>
        <li class="animate fadeIn" data-anim-type="fadeIn" data-anim-delay="400"><img src="images/clientlogo6-gray.png" alt=""></li>
        <li class="animate fadeIn" data-anim-type="fadeIn" data-anim-delay="500"><img src="images/clientlogo4-gray.png" alt=""></li>
        <li class="animate fadeIn" data-anim-type="fadeIn" data-anim-delay="600"><img src="images/clientlogo7-gray.png" alt=""></li>
    </ul>
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec9">
<div class="container transpef">
	
    <div class="title11">
    	<h2 class="white">Need Legal Assistance - Do You Have a Claim?
        <em>Feel free to get in touch with any enquiries and one of our friendly members of staff will get back to you as soon as possible.</em>
        </h2>
	</div>
    
    <div class="cforms">
        
        <form action="http://gsrthemes.com/lawswift/boxed/demo-contacts.php" method="post" id="sky-form" class="sky-form">
          <fieldset>
            <div class="row">
            
              <div class="col col-4">
                <label class="label"><strong>Your Name*</strong></label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </div>
              
              <div class="col col-4">
                <label class="label"><strong>Your E-mail*</strong></label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </div>
            
                        
            <div class="col col-4">
              <label class="label"><strong>Phone Number</strong></label>
              <label class="input"> <i class="icon-append icon-tag"></i>
                <input type="text" name="subject" id="subject">
              </label>
            </div>
            
            </div>
  			
            <br />
             
            <div>
              <label class="label"><strong>Message*</strong></label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="5" name="message" id="message"></textarea>
              </label>
            </div>
            
          </fieldset>
          <footer>
            <button type="submit" class="subbutton">Submit Your Message</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
	</div>
    
	<div class="clearfix margin_top4"></div>
    
</div>
</div><!-- end features section 9 -->

<div class="clearfix"></div>


<footer>

<div class="footer">

    <div class="container">
    
        <div class="left">
        
            <h5>Get Free Consultation</h5>
            <h6>Available 24/7</h6>
            <h3>+91 966 254 7607</h3>
            
        </div><!-- end section -->
        
        <div class="center">
        
            <h5>Message Us Now</h5>
            <h6>Available 24/7</h6>
            <h3><a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a></h3>
        
        </div><!-- end section -->
        
        <div class="right">
        
            <h5>Address Location</h5>
            <h6>315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, Ahmedabad, Gujarat 380006 <br /> <a href="#">View Map</a></h6>


            
        
        </div><!-- end section -->
    
    </div>
    
</div><!-- end footer -->

<div class="copyrights">
<div class="container">

	<div class="one_half">Copyright © 2018 Hem Infotech. All Rights Reserved</div>
	
    <div class="one_half last"><a href="#">Notices</a> | <a href="#">Privacy Policy</a> | <a href="#">Careers</a></div>
    
    
</div>
</div><!-- end copyrights -->

</footer>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>

<!-- mega menu -->
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<!-- MasterSlider -->
<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	 slider.setup('masterslider' , {
		 width:1400,    // slider standard width
		 height:580,   // slider standard height
		 space:0,
		 speed:45,
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"basic"
	});

})(jQuery);
</script>

<!-- scroll up -->
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>

<!-- forms -->
<script src="js/form/jquery.form.min.js"></script>
<script src="js/form/jquery.validate.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";

	$(function()
	{
		// Validation
		$("#sky-form").validate(
		{					
			// Rules for form validation
			rules:
			{
				name:
				{
					required: true
				},
				email:
				{
					required: true,
					email: true
				},
				message:
				{
					required: true,
					minlength: 10
				}
			},
								
			// Messages for form validation
			messages:
			{
				name:
				{
					required: 'Please enter your name',
				},
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				},
				message:
				{
					required: 'Please enter your message'
				}
			},
								
			// Ajax form submition					
			submitHandler: function(form)
			{
				$(form).ajaxSubmit(
				{
					success: function()
					{
						$("#sky-form").addClass('submited');
					}
				});
			},
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
	});			

})(jQuery);
</script>

<!-- cubeportfolio -->
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>

<!-- owl carousel -->
<script src="js/carouselowl/owl.carousel.js"></script>

<!-- tabs -->
<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<script type="text/javascript" src="js/universal/custom.js"></script>

<!-- Accordion-->
<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>


</body>

<!-- Mirrored from gsrthemes.com/lawswift/boxed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Mar 2015 05:47:53 GMT -->
</html>
