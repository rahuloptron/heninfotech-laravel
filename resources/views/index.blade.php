 
@extends('layouts.app')
@section('content')
<div class="mstslider">

<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

    <div class="ms-slide slide-1" data-delay="7">
         
        <!-- slide background -->
        <img src="images/sliders/master/firewall-security.jpg" alt=""/>     
                
        <div class="ms-layer text1 center"
            style="left:380px; top:163px"
            data-effect="bottom(50)"
            data-duration="500"
            data-delay="0"
            data-ease="easeOutExpo"
        >Firewall <br />  Network Security <br /> </div>
          
        <div class="ms-layer sldbut1"
            style="left:595px; top:435px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="600"
            data-ease="easeOutExpo"
        ><a href="contact.html">Get Started Now!</a></div>
                
    </div><!-- end slide -->
    


    <div class="ms-slide slide-2" data-delay="7">
         
        <!-- slide background -->
        <img src="images/sliders/master/amc-services.jpg" alt=""/>     
                
        <div class="ms-layer text1 center"
            style="left:350px; top:163px"
            data-effect="bottom(50)"
            data-duration="500"
            data-delay="0"
            data-ease="easeOutExpo"
        >AMC <br /> Computer  Services <br /> </div>
          
        <div class="ms-layer sldbut1"
            style="left:595px; top:435px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="600"
            data-ease="easeOutExpo"
        ><a href="contact.html">Get Started Now!</a></div>
                
    </div><!-- end slide -->

    <div class="ms-slide slide-3" data-delay="7">
         
        <!-- slide background -->
        <img src="images/sliders/master/home_1_slider_2.jpg" alt=""/>     
                
        <div class="ms-layer text1 center"
            style="left:420px; top:163px"
            data-effect="bottom(50)"
            data-duration="500"
            data-delay="0"
            data-ease="easeOutExpo"
        >Computer <br /> Repair Services <br /> </div>
          
        <div class="ms-layer sldbut1"
            style="left:595px; top:435px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="600"
            data-ease="easeOutExpo"
        ><a href="contact.html">Get Started Now!</a></div>
                
    </div><!-- end slide -->

</div><!-- end of masterslider -->

</div><!-- end slider -->

<div class="clearfix"></div>

<div class="feature_sec1">
<div class="container">

    <div class="title11">
        <h2>Welcome to Hem Infotech - Providing IT Solutions
        <em>Experience. Reliability. Ethics</em>
        <span class="line"></span></h2>
    </div>
    
    <h5>We take pride in introducing ourselves as a professionally managed company which are in selling computers and it’s peripherals , provide technical solution in networking & communication, FMS Services, Laptop Services, Annual Maintenance Contract (AMC). Software implementation , Firewall Solutions,Antivirus , Data Backup Solution and Domain and Email solutions.</h5>
        
    <h5>Get your Consultation <strong>+91 966 254 7607</strong> Available 10:00 AM - 07:00 PM </h5>
    
</div>
</div><!-- end features section 1 -->

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">

    <div class="one_full title8">
    
        <h2><span class="line"></span><span class="text"> <strong>We Offer</strong></span></h2>
        
        
    </div>

     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_third">
    
        <a href="/services/amc-services.html"> <div class="imgframe3"><img src="images/data-recovery2.jpg" alt=""><strong>Amc and Repair Services</strong> </div></a>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
        <a href="/solutions/networking.html"> <div class="imgframe3"><img src="images/networking-new.jpg" alt=""> <strong>Networking Solutions</strong></div></a>
    
    </div><!-- end section -->

   
    <div class="one_third last">
    
      <a href="/products/cctv-on-rent.html">  <div class="imgframe3"><img src="images/cctv-survillance.png" alt=""> <strong>Surveillance and Solutions</strong></div></a>
    
    </div><!-- end section -->
    
 <div class="clearfix margin_top6"></div>

    <div class="one_third">
    
        <a href="/solutions/antivirus-dealer.html"> <div class="imgframe3"><img src="images/antivirus-vs-security.png" alt=""> <strong>Antivirus and Firewall</strong></div></a>
    
    </div><!-- end section -->
    
    
    <div class="one_third">
    
      <a href="/solutions/databackup.html">  <div class="imgframe3"><img src="images/data-backup.png" alt=""> <strong>Data Backup and Storage</strong></div></a> 
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
     <a href="/solutions/thin-client.html">  <div class="imgframe3"><img src="images/thin-client.png" alt=""> <strong>Thin Client</strong></div></a> 
    
    </div><!-- end section -->
    
</div>
</div>


<div class="clearfix"></div>


<div class="content_fullwidth1">
<div class="container">
    
    <div class="counters1">
    
         <div class="one_fourth"> <span id="target5">0</span><span>+</span> <h4>Years</h4> </div>

        <div class="one_fourth"> <span id="target">0</span><span>+</span> <h4>IT Projects</h4> </div>
        
        <div class="one_fourth"> <span id="target2">0</span><span>+</span> <h4>Clients Served</h4> </div>
        
        <div class="one_fourth last"> <span id="target3">0</span><span>k+</span> <h4>Assets Managed</h4> </div>
        
       
        
    </div><!-- end counters1 section -->
    
    
</div>
</div><!-- end content area -->

<div class="clearfix"></div>

<div class="feature_sec13">
<div class="container">
    
    <div class="one_half">
    
        <h3>People <strong>Love Us</strong></h3>
        
        <div id="owl-demo10" class="owl-carousel small three">
    
            <div>
            
                <div class="peoplesays">
                
                    Hem Infotech has been helping us with all our computer related services and solutions for the past five years. We are very much impressed with their punctuality and efficiency.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Eagle Corporation</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We are very glad that we chose Hem Infotech for our computer related services. For the past five years they have been maintaining 105 computers and 10 printers at our office.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Nigam Amin, ICAI</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    We have been associated with Hem Infotech for the past five years and we couldn’t have been happier. They have created an everlasting bond with us with computer maintenance service.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- R. R. Kapadia</strong></div>
                   
            </div><!-- end section -->
            
            <div>
            
                <div class="peoplesays">
                
                    The working of our firm has been made so much smoother with the help of Hem Infotech. We have been using their services for past four years.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- J.M.Chauhan, The Bar Council of Gujarat</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    For the past four years we have given the responsibility of our firms’ computer networking systems to Hem Infotech. We couldn’t have been happier about that decision.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- Confedration of Indian Industry (CII)</strong></div>
                   
            </div><!-- end section -->

              <div>
            
                <div class="peoplesays">
                
                    Hem Infotech helped us with installation and maintenance of 62 computers, 2 laptops and 73 printers. We are completely satisfied with their services.
                </div>
                
                <div class="peoimg"><img src="images/site-img3.jpg" alt="" /> <strong>- C.P. Mathur, Employees Provident Fund Organisation</strong></div>
                   
            </div><!-- end section -->
           
        </div>
        
    </div><!-- end all section -->
    
    
     <div class="one_half last">
    
        <h3><strong>Industry Verticals</strong></h3>
        
                    <ul class="list9">
                    
                    <li><i class="fa fa-long-arrow-right"></i> School and Colleges</li>
                    <li><i class="fa fa-long-arrow-right"></i> Government</li>
                    <li><i class="fa fa-long-arrow-right"></i> Real Estate</li>
                    <li><i class="fa fa-long-arrow-right"></i> Hospitality</li>
                    <li><i class="fa fa-long-arrow-right"></i> Manufacturing</li>
                    <li><i class="fa fa-long-arrow-right"></i> Architects and Graphics</li>
                      
                    </ul>

    </div><!-- end all section -->
    

</div>
</div><!-- end features section 13 -->

<div class="clearfix"></div>

<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong> Esteemed Clients</strong></span></h2>
    
    </div>
    
    <br>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/guj-vidyapith.png" alt="">
            </div>
            
        </div>
    
    </div>


                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gujarat-university.png">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/iit.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/confederation-indian-industry.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/epfo.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>

    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-railway.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/departmentofdefence.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
             <img src="images/clients/IIA.png" alt="">
            </div>
            
        </div>
    
    </div>
        
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/revenue-forest-department-maharashtra.jpg" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/indian-airforce.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/lubi.png" alt="">
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/chocolate-room.jpg">
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ratna-rising.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gulmohar-garden.png" alt="">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/veritas.jpg" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/gsec.jpg" alt="">
            </div>
          
        </div>
    
    </div>
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/sunheart.png" alt="">
            </div>
          
        </div>
    
    </div>
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/ramdev-since.png" alt="">
            </div>
         
        </div>
    
    </div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/suvidha-engineers.jpg">
            </div>
            
        </div>
    
    </div>
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flipscont4">
                <img src="images/clients/eagle-corporation.png" alt="">
            </div>
          
        </div>
    
    </div>


    <div class="margin_top3"></div><div class="clearfix"></div>

    <div><p>And many More...</p></div>
    

</div>
</div>



<div class="clearfix"></div>

<div class="punch_text03">

    <div class="container">
    
        <div class="left">
            <h1>Contact Hem Infotech to Keep Your Business Running</h1>
        </div><!-- end left -->
        
        <div class="right"><a href="contact.html">&nbsp; Request Quote!</a></div><!-- end right -->
    
    </div>

</div>

<div class="clearfix"></div>


    
</div>
</div>

<div class="clearfix"></div> 

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>


@stop