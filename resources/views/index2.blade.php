<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Hem Infotech IT Solution Service Provider</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

    <!-- animations -->
    <link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
  
    
    <!-- mega menu -->
    <link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- MasterSlider -->
	<link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="js/masterslider/skins/default/style.css" />
    
    <!-- owl carousel -->
    <link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">
    
    <!-- forms -->
    <link rel="stylesheet" href="js/form/sky-forms.css" type="text/css" media="all">
    
    <!-- tabs -->
    <link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs2.css">

    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
    
</head>

<body>

<div class="wrapper_boxed">

<div class="site_wrapper">

<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_nav">
        <div class="container">
        	
            <div class="left">
            
            	<ul class="topsocial">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            
            </div><!-- end left links -->
            
            <div class="right">
                
                Call Us: <strong>+91 966 254 7607</strong>       Email: <a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a>
          
        </div><!-- end right links -->
        
        </div>
        </div>
            
 	</div>
    
	</div><!-- end top navigations -->
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
	<nav class="menu_main">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
         <li class="dropdown"><a href="index.html" data-toggle="dropdown" class="dropdown-toggle">home</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="index2.html">home2</a></li>
                <li><a href="index3.html">home3</a></li>
              
            </ul>
        </li>
               
         <li><a href="about.html">About</a></li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Products</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="attorneys.html">Antivirus</a></li>
                <li><a href="attorneys-2.html">Desktops</a></li>
                <li><a href="attorneys-3.html">Server And Workstations</a></li>
                <li><a href="attorneys-4.html">Firewall</a></li>
                <li><a href="attorneys-fullbio.html">CCTV on hire</a></li>
            </ul>
        </li>
        
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">AMC Service</a></li>
                <li><a href="practice-2.html">Data Recovery</a></li>
                <li><a href="practice-3.html">Computer Repair Services</a></li>
                <li><a href="practice-4.html">Resistant Engineer</a></li>
            </ul>
        </li>

        <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Solutions</a>
            <ul class="dropdown-menu multilevel" role="menu">
                <li><a href="practice.html">Data Backup and Storage</a></li>
                <li><a href="practice-2.html">Antivirus And Firewall</a></li>
                <li><a href="practice-3.html">Thin Client</a></li>
                <li><a href="practice-4.html">Active Passive Networking</a></li>
            </ul>
        </li>
        
        <li> <a href="contact.html">Contact Us</a> </li>
        
        </ul>
        
      </div>
      </div>
     </div>
     
	</nav><!-- end Navigation Menu -->
        
	</div>
		
	</div>
    
	</div>
    
</header>


<!-- Slider
======================================= -->  

<div class="mstslider">

<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

    <!-- slide -->
   	<div class="ms-slide slide-1" data-delay="7">
         
        <!-- slide background -->
        <img src="images/sliders/master/slider-1.jpg"  alt=""/>     
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:120px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:116px; top:123px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        >NEED LEGAL <br /> ASSISTANCE?</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:305px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        >Get immediate free information and advice confidential from the <br />
experienced attorneys on most common legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:455px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-2" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>         
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:30px"
            data-effect="bottom(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:116px; top:120px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Making <br /> Your <br /> Priorities <br /> Ours</div>
          
        <div class="ms-layer sldbut1"
        	style="left:120px; top:466px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-3" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>          
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:0px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1 small"
        	style="left:116px; top:113px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Get <br /> confidential <br /> legal advice</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:327px"
            data-effect="left(100)"
            data-duration="1600"
            data-delay="1100"
            data-ease="easeOutExpo"
        >Get free information &amp; advice confidential on most legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:445px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="1300"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    <!-- slide -->
   	<div class="ms-slide slide-4" data-delay="7">
         
        <!-- slide background -->
         <img src="images/sliders/master/slider-1.jpg"  alt=""/>          
        
         <img src="../masterslider/blank.html" alt="" class="ms-layer"
            style="bottom:0; right:0px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1 smalldark"
        	style="left:116px; top:143px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="600"
            data-ease="easeOutExpo"
        >Experienced <br /> Attorneys</div>
        
        <div class="ms-layer text2"
        	style="left:116px; top:305px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Get immediate free information and advice confidential from the <br />
experienced attorneys on most common legal issues.</div>
         
        <div class="ms-layer sldbut1"
        	style="left:116px; top:455px"
            data-effect="right(50)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        ><a href="#">Learn More</a></div>
                
	</div><!-- end slide -->
    
    

</div><!-- end of masterslider -->

</div><!-- end slider -->

<div class="clearfix"></div>

<div class="feature_sec1">
<div class="container">

    <div class="title11">
    	<h2>Welcome to Hem Infotech - Providing IT Services
        <em>Experience .Reliability .Ethics</em>
        <span class="line"></span></h2>
	</div>
	
    <p>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form by injected humour or randomised words which
looks anitime even believable. If you are going to use passage of lorem Ipsum you need to be sure there is anything works lorem ipsum as their default model text and search for loremipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
        
    <h5>Get your Free Consultation <strong class="animate" data-anim-type="fadeIn" data-anim-delay="300">+91 966 254 7607</strong> Available 24/7 </h5>
    
</div>
</div><!-- end features section 1 -->

<div class="clearfix"></div>

<div class="feature_sec4">
<div class="container">
    
    <div class="title11">
    
        <h2> <strong>Solutions</strong>
        <span class="line2"></span></h2>
        
    </div>
    
    <br>
    
    <div id="grid-container" class="cbp-l-grid-fullScreen smallthu cbp cbp-chrome cbp-caption-zoom cbp-animation-fadeOutTop cbp-ready cbp-cols-4" style="height: 417px;">
    
        <ul class="cbp-wrapper" style="opacity: 1;">
        
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(0px, 0px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img5.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-briefcase"></i> <br> <strong>AMC Services</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(296px, 0px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img6.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-home"></i> <br> <strong>Data Recovery</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(592px, 0px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img7.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-medkit"></i> <br> <strong>Computer Repair</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(888px, 0px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img8.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-car"></i> <br> <strong>Data Backup</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(0px, 217px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img9.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-users"></i> <br> <strong>Antivirus</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(296px, 217px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img10.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-edit"></i> <br> <strong>Thin Client</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(592px, 217px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img11.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-money"></i> <br> <strong>Firewall</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
            
            <li class="cbp-item" style="width: 279px; height: 200px; transform: translate3d(888px, 217px, 0px);"><div class="cbp-item-wrapper">
                <a href="#" class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/site-img12.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><i class="fa fa-university"></i> <br> <strong>Servers</strong></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div></li><!-- end item -->
               
        </ul>
    </div>
     
    <div class="cbp-l-loadMore-text">
        <div data-href="ajax/loadMore.html" class="cbp-l-loadMore-text-link"></div>
    </div>
    
</div>
</div>

<div class="clearfix"></div>


<div class="feature_sec12">
<div class="container">
    
    <div class="title11">
        <h2>Why <strong>Choose Us</strong>
        <span class="line4"></span></h2>
    </div>
    
    <br>
    
    
    
    <div class="one_third">
    
        <i class="fa fa-paper-plane-o"></i>
        
        <h5>Specimen book</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
   
    
    <div class="one_third">
    
        <i class="fa fa-thumbs-o-up"></i>
        
        <h5>More obscure</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <i class="fa fa-bell-o"></i>
        
        <h5>Treatise theory</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="clearfix margin_top6"></div>
    
   
    
    <div class="one_third">
    
        <i class="fa fa-heart-o"></i>
        
        <h5>Versions from</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_third">
    
        <i class="fa fa-bookmark-o"></i>
        
        <h5>Default model</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <i class="fa fa-envelope-o"></i>
        
        <h5>Always free</h5>
        
        <p>Simply dummy text of the printing type industry.</p>
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="counters2">
    
        <div class="one_fourth"> <i class="fa fa-paper-plane"></i> <span id="target6">270</span> <h4>Projects</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-umbrella"></i> <span id="target7">225</span> <h4>Clients</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-trophy"></i> <span id="target8">90</span> <h4>Awards</h4> </div>
        
        <div class="one_fourth last"> <i class="fa fa-heart"></i> <span id="target9">4500</span> <h4>Likes</h4> </div>
        
       
    </div><!-- end counters2 section -->
    
</div>
</div>
<div class="clearfix"></div>

<div class="feature_sec10">
<div class="container">
    
    <div class="title11">
    
        <h2>Areas of <strong>Practice</strong>
        <span class="line2"></span></h2>
        
    </div>
    
    <br>
    
    <div id="owl-demo6" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
    
            <div class="owl-wrapper-outer autoHeight" style="height: 338px;"><div class="owl-wrapper" style="width: 11700px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(-3510px, 0px, 0px); transform-origin: 4095px center 0px; perspective-origin: 4095px center;"><div class="owl-item" style="width: 1170px;"><div>
            
                <div class="one_half"><img src="images/site-img29.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Corporate and Securities</h3>
                    
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
<br>
<p>Many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div></div><div class="owl-item" style="width: 1170px;"><div>
            
                <div class="one_half"><img src="images/site-img30.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Medical Malpractice</h3>
                    
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using <a href="#"><strong>Lorem Ipsum</strong></a> is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
<br>
<p>Many variations of passages of <span class="lable3">Lorem Ipsum available</span>, but the majority have suffered alteration in some form by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div></div><div class="owl-item" style="width: 1170px;"><div>
            
                <div class="one_half"><img src="images/site-img31.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Vehicle Accidents</h3>
                    
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as <span class="lable2">their default model text</span>, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
<br>
<p>Many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div></div><div class="owl-item" style="width: 1170px;"><div>
            
                <div class="one_half"><img src="images/site-img32.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Family Law</h3>
                    
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
<br>
<p>Many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div></div><div class="owl-item" style="width: 1170px;"><div>
            
                <div class="one_half"><img src="images/site-img33.jpg" alt=""></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Real Estate &amp; Land</h3>
                    
                    <p>Long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more less normal distribution of letters, as opposed to using here, content here, making it look like readable English. <strong class="color2">Many desktop publishing packages</strong> and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy versions have over the years.</p>
<br>
<p>Many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div></div></div></div><!-- end section -->
            
            <!-- end section -->
            
            <!-- end section -->
            
            <!-- end section -->
            
            <!-- end section -->
              
        <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>

</div>
</div>

<div class="clearfix"></div>
<div class="feature_sec3">
<div class="container">
	
    <div class="one_half animate" data-anim-type="fadeIn" data-anim-delay="500">
    	
        <h2 class="small"><strong>FAQ</strong></h2>

        <div id="st-accordion-four" class="st-accordion-four">
        <ul>
            <li class="" style="overflow: hidden; height: 50px;">
                <a href="#">4 Diffrent Creative Header Styles<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>She packed her seven versalia, put her initial into the belt and made on the way.</p>
                    <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.</p>

                </div>
            </li>
            <li>
                <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>

              </div>
            </li>
            <li>
                <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>O my friend - but it is too much for my strength - I sink under the weight of the splendour of these visions!</p>
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
                </div>
            </li>
            <li>
                <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                <div class="st-content" style="display: none;">
              <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?"</p>
                    <p>He thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
                </div>
            </li>
            
        </ul>
    </div>
        
    	
    </div><!-- end all section -->
    
    
    <div class="one_half last hpeosays animate" data-anim-type="fadeIn" data-anim-delay="700">
    	
        <h2 class="small">Why Clients <strong>Love Us</strong></h2>

        <div id="owl-demo3" class="owl-carousel small">
    
            <div>
            
            	<img src="images/site-img1.jpg" alt="" />
                
                <p><strong>Lorem that more</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Ricky Holness, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img3.jpg" alt="" />
                
                <p><strong>Packages that</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Jean Desmond, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img2.jpg" alt="" />
                
                <p><strong>Apposed to using</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Devin Braedon, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
            
            <div>
            
            	<img src="images/site-img4.jpg" alt="" />
                
                <p><strong>Webpage editors</strong> less normal distribution of letters as opposed using content here content makin itlook like readable english many web page editors now used then model sentence structures the to generate which looks.</p>
                
                <b>- Cason Harrison, <em>additional info</em></b>
                
                <div class="divider_line21 last"></div>
                
            </div><!-- end section -->
               
		</div>
	</div><!-- end all section -->
	
    
     
</div>
</div><!-- end features section 3 -->

<div class="clearfix"></div>



<div class="feature_sec14">
<div class="container">

    <div class="title8">
    
        <h2><span class="line"></span><span class="text">Our <strong>Clients</strong><span></span></span></h2>
    
    </div><!-- end section title heading -->
    
    <br>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/1.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Microsoft</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/2.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">ebay</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/3.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Adobe</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/4.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">IBM</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/5.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Google</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/17.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Yahoo</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/7.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Discovery</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/8.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">envato</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/9.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Deloitte</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/20.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Audi</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->

    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/11.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Yahoo</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
                            
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/12.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Discovery</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/13.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">envato</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/14.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Deloitte</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fifth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/16.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Audi</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_sec9">
<div class="container transpef">
	
    <div class="title11">
    	<h2 class="white">Need Legal Assistance - Do You Have a Claim?
        <em>Feel free to get in touch with any enquiries and one of our friendly members of staff will get back to you as soon as possible.</em>
        </h2>
	</div>
    
    <div class="cforms">
        
        <form action="http://gsrthemes.com/lawswift/boxed/demo-contacts.php" method="post" id="sky-form" class="sky-form">
          <fieldset>
            <div class="row">
            
              <div class="col col-4">
                <label class="label"><strong>Your Name*</strong></label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </div>
              
              <div class="col col-4">
                <label class="label"><strong>Your E-mail*</strong></label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </div>
            
                        
            <div class="col col-4">
              <label class="label"><strong>Phone Number</strong></label>
              <label class="input"> <i class="icon-append icon-tag"></i>
                <input type="text" name="subject" id="subject">
              </label>
            </div>
            
            </div>
  			
            <br />
             
            <div>
              <label class="label"><strong>Message*</strong></label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="5" name="message" id="message"></textarea>
              </label>
            </div>
            
          </fieldset>
          <footer>
            <button type="submit" class="subbutton">Submit Your Message</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
	</div>
    
	<div class="clearfix margin_top4"></div>
    
</div>
</div><!-- end features section 9 -->

<div class="clearfix"></div>


<footer>

<div class="footer">

    <div class="container">
    
        <div class="left">
        
            <h5>Get Free Consultation</h5>
            <h6>Available 24/7</h6>
            <h3>+91 966 254 7607</h3>
            
        </div><!-- end section -->
        
        <div class="center">
        
            <h5>Message Us Now</h5>
            <h6>Available 24/7</h6>
            <h3><a href="mailto:sales@heminfotech.com">sales@heminfotech.com</a></h3>
        
        </div><!-- end section -->
        
        <div class="right">
        
            <h5>Address Location</h5>
            <h6>315, Devnanadan Mall, Opp. Sansyas Ashram, Ellisbridge, Ahmedabad, Gujarat 380006 <br /> <a href="#">View Map</a></h6>


            
        
        </div><!-- end section -->
    
    </div>
    
</div><!-- end footer -->

<div class="copyrights">
<div class="container">

	<div class="one_half">Copyright © 2018 Hem Infotech. All Rights Reserved</div>
	
    <div class="one_half last"><a href="#">Notices</a> | <a href="#">Privacy Policy</a> | <a href="#">Careers</a></div>
    
    
</div>
</div><!-- end copyrights -->

</footer>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>

<!-- mega menu -->
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<!-- MasterSlider -->
<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	 slider.setup('masterslider' , {
		 width:1400,    // slider standard width
		 height:580,   // slider standard height
		 space:0,
		 speed:45,
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"basic"
	});

})(jQuery);
</script>

<!-- scroll up -->
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>

<!-- forms -->
<script src="js/form/jquery.form.min.js"></script>
<script src="js/form/jquery.validate.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";

	$(function()
	{
		// Validation
		$("#sky-form").validate(
		{					
			// Rules for form validation
			rules:
			{
				name:
				{
					required: true
				},
				email:
				{
					required: true,
					email: true
				},
				message:
				{
					required: true,
					minlength: 10
				}
			},
								
			// Messages for form validation
			messages:
			{
				name:
				{
					required: 'Please enter your name',
				},
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				},
				message:
				{
					required: 'Please enter your message'
				}
			},
								
			// Ajax form submition					
			submitHandler: function(form)
			{
				$(form).ajaxSubmit(
				{
					success: function()
					{
						$("#sky-form").addClass('submited');
					}
				});
			},
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
	});			

})(jQuery);
</script>

<!-- cubeportfolio -->
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>

<!-- owl carousel -->
<script src="js/carouselowl/owl.carousel.js"></script>

<!-- tabs -->
<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<script type="text/javascript" src="js/universal/custom.js"></script>

<!-- Accordion-->
<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>


</body>

<!-- Mirrored from gsrthemes.com/lawswift/boxed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 19 Mar 2015 05:47:53 GMT -->
</html>
